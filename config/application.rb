require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Organisator
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    
    # set the default locale -- magnus
    config.i18n.default_locale = :de
    
    # set the time zone
    config.time_zone = 'Europe/Berlin'
    
    config.action_view.sanitized_allowed_attributes = %w( abbr alt cite class data-turbolinks datetime height href lang name src target title width xml:lang )
  end
  
end
