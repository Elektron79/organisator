Rails.application.routes.draw do
  
  controller :community do
    get 'community', to: 'community#index'
    post 'community/search', to: 'community#search'
    get 'community/last_week', to: 'community#last_week', as: :last_week_community
    get 'community/:plz', to: 'community#show', as: :show_community
  end
  
  resources :taggings
  
  resources :tags do
    member do
      put :add
      put :orphanize
    end
  end
  
  resources :posts
  
  resources :participations do
    member do
      get :activate
    end
  end
  
  resources :streams do
    resources :participations
    resources :posts
  end
  
  resources :orderquants
  
  resources :orders do
    member do
      get :activate
    end
  end
  
  resources :donations do
    member do
      put :advance
    end
  end
  
  resources :answers
  
  resources :questions do
    resources :answers
  end
  
  resources :rewards do
    resources :donations
  end
  
  resources :fundings do
    resources :questions
    resources :rewards
    resources :donations do
      member do
        put :new
      end
    end
    
    member do
      put :publish
    end
  end
  
  controller :statistics do
    get 'statistics/index' => :index
    get 'statistics/current_requests' => :current_requests
  end
  
  resources :prefs do
    collection do
      get :overview
    end
    
    member do
      put :switch
    end
  end
  
  resources :votes
  
  resources :comments
  
  resources :arguments
  
  resources :proposals do
    resources :comments
    
    member do
      put :vote
    end
  end
  
  resources :decisions do
    resources :proposals
    
    member do
      put :publish
      put :vote
      get :result
    end
  end
  
  resources :messages
  
  resources :memberships do
    member do
      patch :accept
      get :activate
      get :show_summary
      get :requestpdf
    end
      
    collection do
      get :export
    end
  end
  
  resources :registrations do
    member do
      get :activate
    end
  end
  
  resources :pages do
    member do
      put :switchlock
    end
  end
  
  resources :contentparts do
    member do
      put :up
      put :to_top
      put :down
      put :to_bottom
    end
  end
  
  resources :newsletters do
    member do
      put :ready
      post :test
    end
  end
  
  resources :articles do
    member do
      put :publish
    end
  end
  
  resources :events do
    resources :registrations
  end
  
  resources :contacts do
    collection do
      get :email_address
    end
    
    member do
      get :activate
      get :unsubscribe
    end
    
    resources :messages
  end
  
  controller :sessions do
    get 'login' => :new
    post 'login'=> :create
    delete 'logout' => :destroy
  end
  
  resources :accounts do
    collection do
      match :build, via: %i( get patch )
      match :check, via: [ :get, :post ]
      match :request_password, via: [ :get, :post ]
      get :mine
    end
    member do
      match :reset_password, via: [ :get, :patch ]
    end
  end
  
  # Admin namespace
  
  namespace :admin do
    resources :accounts do
      member do
        patch :charm
      end
    end
    
    resources :memberships do
      resources :shares
      resources :payments
    end
    
    resources :shares do
      member do
        put :allocate
      end
    end
    
    resources :payments
  end
  
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get '/search', to: 'application#search'
  get '/impressum', to: 'pages#impressum'
  get '/datenschutz', to: 'pages#datenschutz'
  
  # catch all route for database pages
  get '/*path', to: 'pages#show',
      constraints: lambda { |req|
          req.path.exclude? 'rails/active_storage' }
  
  root to: 'pages#root'
  
end
