# Be sure to restart your server when you modify this file.

# Naming of your organisation and her domain
Rails.application.config.orga_name = 'Organisator'
Rails.application.config.orga_domain = 'organisator.org'
Rails.application.config.orga_slogan = 'WebApp für Organisationen'
Rails.application.config.orga_color = '#d9d9d9'
