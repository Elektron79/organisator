# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ membership_search.coffee'
  
  show_search_form = ( e ) ->
    $search_form = ($ '#membership_search_form')
    
    console.log "-- TOGGLE!"
    $search_form.toggleClass 'initially_hidden'
    $search_form.find( 'input#label' ).focus()
    
    e.stopPropagation();
  
  
  process_search_form_inputs = ( e ) ->
    console.log '> script @ membership_search.coffee'
    
    $ich = ($ this)
    target = e.target
    input_string = target.value
    console.log "-- Input: #{input_string}"
    
    for elem in ($ '.results_block li')
      #console.log( elem )
      #console.log( "- elem: #{($ elem).prop 'class'}" )
      #console.log( "- elem: #{($ elem).data 'label'}" )
      
      $elem = ($ elem)
      label = $elem.data( 'label' ).toLowerCase()
      if input_string.length > 2 and label.includes( input_string.toLowerCase() )
        #console.log( "- show" )
        $elem.show()
      else
        #console.log( "- hide" )
        $elem.hide()
      
    
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_membership_search'
    console.log '-- init'
    
    $main.on 'click.membership_search_toggle',
        '#add_tag_button',
        show_search_form
    ($ '#membership_search_form').on 'input.tagger',
        'input#payment_membership_token',
        process_search_form_inputs
    
    $main.data 'init_tagger', true
  

