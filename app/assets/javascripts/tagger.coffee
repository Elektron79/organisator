# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ tagger.coffee'
  
  show_tagger_modal = ( e ) ->
    $ich = ($ this)
    $tagger_modal = ($ '#tagger_modal')
    
    console.log "-- TOGGLE!"
    $tagger_modal.toggleClass 'toggled_open'
    $ich.toggleClass 'open'
    $tagger_modal.find( 'input#label' ).focus()
    
    e.stopPropagation();
  
  
  process_tagger_inputs = ( e ) ->
    console.log '> script @ tagger.coffee'
    
    $ich = ($ this)
    target = e.target
    input_string = target.value
    console.log "-- Input: #{input_string }"
    
    $li = ($ '#create_new_tag_tagging_form')
    $li.data 'label', input_string
    $li.find( 'input[type=submit]' ).prop( 'value', input_string )
    $li.find( 'input[name="tag[label]"]' ).prop( 'value' , input_string )
    
    for elem in ($ '.results_block li')
      #console.log( elem )
      #console.log( "- elem: #{($ elem).prop 'class'}" )
      #console.log( "- elem: #{($ elem).data 'label'}" )
      
      $elem = ($ elem)
      label = $elem.data( 'label' ).toLowerCase()
      if input_string.length > 0 and label.includes( input_string.toLowerCase() )
        #console.log( "- show" )
        $elem.show()
      else
        #console.log( "- hide" )
        $elem.hide()
      
    
  
  
  close_tagger_modal = ( e ) ->
    $ich = ($ this)
    $tagger_modal = ($ '#tagger_modal')
    
    console.log "-- CLOSE!"
    $tagger_modal.removeClass 'toggled_open'
    $ich.removeClass 'open'
    
    e.stopPropagation();
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_tagger'
    console.log '-- init'
    
    $main.on 'click.tagger',
        '#add_tag_button',
        show_tagger_modal
    ($ '#tagger_modal').on 'input.tagger',
        'input#label',
        process_tagger_inputs
    ($ '#tagger_modal').on 'click.tagger',
        '.close_window_button',
        close_tagger_modal
    
    $main.data 'init_tagger', true
  

