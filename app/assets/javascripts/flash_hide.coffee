# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ flash_hide.coffee'
  
  flash_hide = ->
    ($ '#flash').addClass 'faded'
  
  
  flash_hide_back = ->
    ($ '#flash_layer').addClass 'faded'
  
  
  # --- Initialisierung ------------------------
  
  # adjust flash to hide after few seconds
  flash_timer = window.setTimeout flash_hide, 1000
  flash_back_timer = window.setTimeout flash_hide_back, 500

