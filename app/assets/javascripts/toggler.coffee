# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ toggler.coffee'
  
  toggle = ( e ) ->
    $ich = ($ this)
    console.log "-- toggle - #{$ich.attr 'class'} @ toggler.coffee"
    target = ($ e.target)
    console.log "-- e.target: #{target.prop 'tagName'}.#{target.prop 'class'}##{target.prop 'id'}"
    console.log "-- e.type: #{e.type}"
    
    sel = $ich.data 'sel'
    
    if target.closest( '.will_toggle' ).length > 0 and !e.isPropagationStopped()
      console.log "-- TOGGLE!"
      ($ sel).toggleClass 'toggled_open'
      $ich.toggleClass 'open'
      
      # focus first available input text
      el = ($ sel).find( 'input[type=text]' )[0]
      el.focus() if el?
      el = ($ sel).find( 'textarea' )[0]
      el.focus() if el?
        
      e.stopPropagation();
    
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_toggler'
    console.log '-- init'
    $main.on 'click.toggler',
        '[data-action=toggle]',
        toggle
    $main.data 'init_toggler', true
  

