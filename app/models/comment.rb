class Comment < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  # --- RELATIONS -----------------------------
  
  belongs_to :account
  belongs_to :entity, polymorphic: true
  
  # --- SCOPES --------------------------------
    
  scope :newest, -> {
      order updated_at: :desc }
  
  # --- METHODS -------------------------------
  
  def iid
    "cm_#{token}"
  end
  
end
