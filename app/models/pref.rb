class Pref < ApplicationRecord
  
  def iid
    "prf_#{id}"
  end
  
  def active?
    case
      when is_boolean?
        value == 'true'
      when is_string?
        !value.blank?
      when is_number?
        !value.blank?
    end
  end
  
  def is_boolean?
    klass == 'Boolean'
  end
  def is_string?
    klass == 'String'
  end
  def is_number?
    klass == 'Number'
  end
  
  def klassed_value
    case
      when is_boolean? then value == 'true'
      when is_number? then value.to_f
      else value
    end
  end
  
  def switch_value
    if value == 'true'
      self.value = 'false'
    else
      self.value = 'true'
    end
    return value
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.for_status?( in_status )
    status = Account.all_status_below( in_status )
    self.where( level: status ).size > 0
  end
  
  def self.app_feature?( in_feature )
    pref = self.find_by key: in_feature.to_s
    return ( pref and pref.active? )
  end
  
  def self.app_value( in_feature )
    pref = self.find_by key: in_feature.to_s
    return nil unless pref
    pref.klassed_value
  end
  
end
