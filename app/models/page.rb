class Page < ApplicationRecord
  
  belongs_to :parent, class_name: 'Page', optional: true
  has_many :pages, -> { order 'position' }, foreign_key: :parent_id
  has_many :contentparts, -> { order( position: :asc ) }, as: :container, dependent: :destroy
  
  # --- SCOPES --------------------------------
  
  scope :published, -> {
        where status: 'published' }
  scope :root_level, -> {
        where parent_id: nil }
  scope :in_menu, -> {
        where 'CHAR_LENGTH( menu ) > 0' }
  scope :out_of_menu, -> {
        where 'CHAR_LENGTH( menu ) = 0' }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "pag_#{id}"
  end
  
  def activate_kind
    !kind.blank?
  end
  def activate_kind=( in_value )
    logger.debug "-- #{in_value}" 
    logger.debug "-- #{in_value.to_i == 0}" 
    kind = nil if in_value.to_i == 0
  end
  
  def preview_text
    title
  end
  
  def partial_name
    path.gsub '/', '_'
  end
  
  def link_url
    # http works with both http:// and https://
    return path if path.start_with? 'http'
    return path if path.start_with? '/'
    return "/#{path}"
  end
  
  # --- Predicates -----------------------------
  
  def published?
    status == 'published'
  end
  
  def private?
    !published?
  end
  
  def resource?
    title.blank?
  end
  
  def show_to?( in_status = 'public' )
    return true if [ 'admin', 'editor' ].include? in_status
    published?
  end
  
  def accessible_by?( in_account = nil )
    return true if published?
    return false if in_account.blank?
    
    return case status
      when 'confirmed' then in_account.is_confirmed?
      when 'team' then in_account.is_team?
      when 'member' then in_account.is_member?
      when 'team_member' then in_account.is_team_or_member?
      when 'editor' then in_account.is_editor?
      when 'admin' then in_account.is_admin?
      else in_account.is_admin?
    end
  end
  
  def has_picture?
    contentparts.with_pic.count > 0
  end
  
  def unlocked?
    unlocked_at and
        unlocked_at > Time.zone.now - UNLOCK_TIMESPAN
  end
  
  # --- Picture Variants -----------------------
  
  def picture
    contentparts.
        with_pic.
        order( :position ).
        detect { |c|
            c.picture.attached? }
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.select_status
    STATUS.collect { |s| [ I18n.t( "activerecord.attributes.page.status.#{s}" ), s ] }
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS = [ 'new', 'published', 'confirmed', 'team', 'member', 'team_member', 'editor', 'admin' ]
  
  UNLOCK_TIMESPAN = 2.hours
  
end
