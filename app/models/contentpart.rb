class Contentpart < ApplicationRecord
  include Tokenize
  include Picturize
  
  after_initialize :fill_in_token
  after_initialize :setup_picture_variant
  
  belongs_to :container, polymorphic: true
  has_one_attached :picture
  
  acts_as_list scope: :container
  
  # --- SCOPES --------------------------------
  
  scope :with_pic, -> { where kind: [ 'img', 'p_img', 'sized_img' ] }
  
  # --- PREDICATES ----------------------------
  # --- INSTANCE METHODS ----------------------
  
  def iid
    "cp_#{token}"
  end
  
  def to_param
    token
  end
  
  def tag_type
    case kind
      when 'p_img' then 'p'
      when 'audio' then 'p'
      when 'counter_block' then 'div'
      when 'buttons_block' then 'div'
      else kind
    end
  end
  
  def klass
    [ 'contentpart', kind ].join ' '
  end
  
  def own_partial?
    %w( p sized_img img p_img ul ol tile info back_button pdf audio video ).include? kind
  end
  
  def setup_picture_variant
    @picture_variant = {}
  end
  
  # --- Predicates ----------------------------
  
  def is_picture_kind?
    logger.debug "> is_picture_kind? @ contentpart - kind:#{kind}"
    [ 'img', 'p_img', 'sized_img' ].include? kind
  end
  
  def has_picture?
    is_picture_kind? and picture.attached?
  end
  
  # --- CLASS METHODS -------------------------
  
  def self.select_kind
    KINDS
  end
  
  # --- CONSTANTS -----------------------------
  
  KINDS = [
    [ 'Text-Absatz', 'p' ],
    [ 'Bild mit fester Grösse', 'sized_img' ],
    [ 'Bild dynamisch', 'img' ],
    [ 'Bild und Text nebeneinander', 'p_img'],
    [ 'Auflistung ul', 'ul' ],
    [ 'Auflistung ol', 'ol' ],
    [ 'Überschrift h2', 'h2' ],
    [ 'Überschrift h3', 'h3' ],
    [ 'Überschrift h4', 'h4' ],
    [ 'Kachel mit Inhalt', 'tile' ],
    [ 'Zitat', 'blockquote' ],
    [ 'Infobox', 'info' ],
    [ 'Exkurs', 'aside' ],
    [ 'Tasten Block', 'buttons_block' ],
    [ 'Zurück Taste oben', 'back_button' ],
    [ 'PDF Dokument', 'pdf' ],
    [ 'Audio', 'audio' ],
    [ 'Video', 'video' ],
    [ 'Zähler Block', 'counter_block' ]
  ]
  
end
