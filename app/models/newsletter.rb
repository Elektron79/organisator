class Newsletter < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  has_many :contentparts, -> { order( position: :asc ) }, as: :container, dependent: :destroy
  has_many :mailings, dependent: :destroy, as: :packet
  
  # --- SCOPES ---------------------------------
  
  scope :ready, -> {
        where( status: 'ready' )
        .order 'send_at' }
  scope :for_mailing, -> {
        ready
        .where( [ 'send_at < ?',
                Time.zone.now ] )
        .order 'send_at' }
  scope :published, -> {
        where status: 'done' }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "NeB##{id}"
  end
  def debug_text
    "#{iid}: #{status} - #{senden_at.strftime '%d.%m.%y' }"
  end
  def to_param
    "#{token}-#{send_at.strftime '%Y-%m-%d'}"
  end
  
  def name_text
    "Newsletter vom #{senden_at.strftime '%d.%m.%y'}"
  end
  def status_text
    status.to_s.titleize
  end
  
  def klass
    status.to_s.downcase
  end
  
  def lines
    content.split( "\n" )
  end
  def teaser
    lines.first
  end
  
  def last_sent_at
    last_newsletter = Newsletter.
        where( 'send_at < ?', send_at ).
        order( send_at: :desc ).
        first
    return BEGINNING_OF_TIME if last_newsletter.blank?
    return last_newsletter.send_at
  end
  
  def picture_contentpart
    contentparts.with_pic.
        order( :position ).
        detect { |c|
            c.has_picture? }
  end
  
  # --- PREDICATES -----------------------------
  
  def new?
    status == 'new'
  end
  def ready?
    status == 'ready'
  end
  def published?
    status == 'done' and sent?
  end
  def not_sent_yet?
    new? or
        ( ready? and send_at > Time.zone.now and mailings.blank? )
  end
  def sent?
    mailings.size > 0 and
        mailings.any? { |m| m.sent? }
  end
  
  def has_picture?
    contentparts.with_pic.count > 0 and
        contentparts.with_pic.any? { |c| c.has_picture? }
  end
  
  # --- ACTIONS --------------------------------
  
  def start_mailing
    logger.debug "> start_mailing @ newsletter"
    
    # Status vorläufig nach senden, um Doppelungen
    # zu vermeiden
    self.update_attribute :status, 'sending'
    start = Time.zone.now + TIME_TO_WAIT_BEFORE_SENDING
    
    logger.debug "-- Contact.count:#{Contact.count}"
    Contact.wants_news.each_with_index do |n, idx|
      Mailing.create contact: n,
            packet: self,
            status: 'ready',
            send_at: start + ( idx * 2 ).seconds
    end
    
    # Versand ist generiert,
    # Status automatisch nach frei
    self.update_attribute :status, 'done'
  end
  
  def send_out( in_mailing )
    logger.debug "> send_out @ newsletter -- in_mailing:#{in_mailing.to_yaml}"
    ContactsMailer.newsletter_mail( in_mailing ).deliver_later
  end
  
  # --- CONSTANTS ------------------------------
  
  BEGINNING_OF_TIME = Date.new( 2001, 1, 1 )
  TIME_TO_WAIT_BEFORE_SENDING = 3.minutes
  
end
