class Share < ApplicationRecord
  
  # --- RELATIONS ------------------------------
  
  belongs_to :membership
  
  # --- VALIDATIONS ----------------------------
  
  validates_presence_of :membership
  validates_numericality_of :count, :amount
  
  # --- SCOPES ---------------------------------
  
  scope :paid, -> {
      where "bought_at IS NOT NULL" }
  
  # --- INSTANCE METHODS -----------------------
  
  def sort_date
    return sold_at if sold_at
    return bought_at if bought_at
    return ordered_at if ordered_at
    created_at
  end
  
  def sum_of_money
    count * amount
  end
  
  def allocatable?
    bought_at.blank? and membership.saldo_at >= sum_of_money
  end
  
end
