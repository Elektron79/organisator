class Log < ApplicationRecord
  belongs_to :actor, polymorphic: true, optional: true
  belongs_to :entity, polymorphic: true, optional: true
  
  scope :for_cockpit, -> {
        where( 'created_at > ?', Time.zone.now - 2.days )
        .order( 'created_at DESC' )
        .reject { |l| l.ip.blank? }
        .reject { |l| l.from_bot? } }
  scope :for_cockpit_refresh, -> {
        where( 'created_at > ?', Time.zone.now - 3.hour - Time.zone.now.min.minutes - Time.zone.now.sec.seconds )
        .order( 'created_at DESC' )
        .reject { |l| l.ip.blank? }
        .reject { |l| l.from_bot? } }
  
  # --- INSTANCE METHODS -----------------------
  
  def from_bot?
    user_agent =~ BOT_SIGNATURES
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.requests_between( in_time_a, in_time_b )
    self.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: in_time_a, t2: in_time_b ]
  end
  
  # --- CONSTANTS ------------------------------
  
  BOT_SIGNATURES = /Bot|bot|Crawl|crawl|Spider|spider|Datanyze|urllib|tweeted|PaperLi|ubermetricsTrendsmap|Twitterbot|curl|Go\-http|Mediatoolkitbot|check_http|DotBot|YandexBot|Baiduspider|Googlebot|bingbot|TurnitinBot|CareerBot|Ezooms|Exabot|oBot|SiteExplorer|Sosospider|MJ12bot|NetcraftSurveyAgent|Infohelfer|AhrefsBot|SISTRIX Crawler|Statsbot|CMS Crawler|Netcraft Web Server Survey|Snipebot|betaBot|spbot|Yahoo\! Slurp|AppEngine-Google|Java\/1\.\d\.|DuckDuckGo|nutch\-1|SEOkicks\-Robot/
end
