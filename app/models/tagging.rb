class Tagging < ApplicationRecord
  belongs_to :tag
  belongs_to :entity, polymorphic: true
end
