class Article < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  belongs_to :author, class_name: 'Account'
  belongs_to :parent, polymorphic: true, optional: true
  
  has_many :contentparts, -> { order( position: :asc ) }, as: :container, dependent: :destroy
  has_many :taggings, as: :entity, dependent: :destroy
  has_many :tags, -> { order( label: :asc ) }, as: :entity, through: :taggings
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :title
  validates_length_of :description, maximum: 250
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where( status: 'published' ).
        where( 'published_at < ?', Time.zone.now ).
        order( published_at: :desc ) }
  scope :with_parent, -> {
        where.not( parent: nil ) }
  scope :general, -> {
        where parent: nil }
  scope :new_since, -> ( d ) {
        published.
        where( 'updated_at > ?', d ).
        order( published_at: :desc ) }
  scope :for_mail_between, -> ( from, to ) {
        where( status: :published ).
        where( 'updated_at > ?', from ).
        where( 'published_at < ?', to ).
        order( published_at: :desc ) }
  scope :latest, -> ( n ) {
        order( published_at: :desc )[ 0..(n-1) ] }
  scope :page, -> ( n ) {
        order( published_at: :desc ).
        limit( PAGE_SIZE ).
        offset( n * PAGE_SIZE ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}-#{title.parameterize}"
  end
  
  def preview_text
    title
  end
  
  def picture_contentpart
    contentparts.with_pic.
        order( :position ).
        detect { |c|
            c.has_picture? }
  end
  
  # --- PREDICATES -----------------------------
  
  def has_picture?
    contentparts.with_pic.count > 0 and
          contentparts.with_pic.any? { |c| c.has_picture? }
  end
  
  def published?
    status == 'published'
  end
  
  def show_to?( in_status = 'public' )
    return true if [ 'admin', 'editor' ].include? in_status
    published?
  end
  
  # --- CONSTANTS ------------------------------
  
  PAGE_SIZE = 10
  
end
