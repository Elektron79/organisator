class Membership < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  # --- RELATIONS ------------------------------
  
  belongs_to :contact
  accepts_nested_attributes_for :contact
  has_one_attached :request
  
  has_many :payments, -> { order 'paid_at DESC' }, dependent: :nullify
  has_many :shares, -> { order 'bought_at DESC' }, dependent: :destroy
  accepts_nested_attributes_for :shares
  
  has_many :logs, -> {
      where( "action NOT LIKE 'show_%' AND action NOT LIKE 'new_%' AND action NOT LIKE 'edit_%'" ).
      order 'created_at DESC' }, as: :entity
  
  # --- VALIDATIONS ----------------------------
  
  validates_uniqueness_of :token,
        on: :create
  validates_presence_of :token, :contact
  # validates_presence_of :fee_amount, :fee_period, :fee_mode, :recommendation, :phone_nr
  # validates_presence_of :account_owner, :iban,
  #    if: Proc.new { |m| m.fee_mode == 'direct_debit' }
  # validates_acceptance_of :accepts_debit,
  #    if: Proc.new { |m| m.fee_mode == 'direct_debit' }
  validates_acceptance_of :accepts_rules,
      :wants_membership
  
  # --- SCOPES ---------------------------------
  
  scope :preliminary, -> {
      where status: 'preliminary' }
  scope :completed, -> {
      where status: 'completed' }
  scope :unconfirmed, -> {
      references( :contact ).
      includes( :contact ).
      where( "contacts.status IN ( ? )", Contact::LESSER_STATUS ) }
  scope :confirmed, -> {
      where status: 'confirmed' }
  scope :status_new, -> {
      where status: 'new' }
  scope :accepted, -> {
      where status: 'accepted' }
  scope :cancelled, -> {
      where status: 'cancelled' }
  scope :ended, -> {
      where status: 'ended' }
  scope :want_newsletter, -> {
      references( :contact ).
      includes( :contact ).
      where( "contacts.wants_news = true" ) }
  scope :need_fee_check, -> {
      where( 'fee_check_at < ?', Time.zone.now ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def sort_date
    if starts_at
      starts_at.at_midnight
    else
      created_at.at_midnight
    end
  end
  
  def status_text
    return 'discharged' if discharged?
    return status
  end
  
  def preview_text
    "Mitglied #{token}: #{contact.name_text}"
  end
  
  def street_nr_text
    [ street, housenr ].compact.join ' '
  end
  
  def plz_city_text
    [ plz, city ].compact.join ' '
  end
  
  def entry_fee_paid?
    return true unless Pref.app_feature? :membership_entry_fee
    payments.sum( :amount ) >= Pref.app_value( :membership_entry_fee ).to_f
  end
  
  def shares_count
    shares.sum :count
  end
  
  def shares_volume
    shares.inject( 0 ) { |sum,s|
        sum + s.sum_of_money }
  end
  
  def paid_shares_volume
    shares.paid.inject( 0 ) { |sum,s|
        sum + s.sum_of_money }
  end
  
  def complete_volume
    complete = shares_volume
    if Pref.app_feature? :membership_entry_fee
      complete += Pref.app_value( :membership_entry_fee ).to_f
    end
    return complete
  end
  
  def saldo_at( in_date = Time.zone.now )
    logger.debug "-- saldo_at @ membership - #{in_date}"
    sum_paid = payments.up_to( in_date ).sum :amount
    #logger.debug "-- sum_paid:#{sum_paid}"
    sum_paid -= Pref.app_value( :membership_entry_fee ).to_f if Pref.app_feature?( :membership_entry_fee )
    #logger.debug "-- sum_paid minus:#{sum_paid}"
    sum_paid -= shares.paid.inject( 0 ) { |sum,s|
          sum + s.sum_of_money }
    #logger.debug "-- sum_paid minusminus:#{sum_paid}"
    return sum_paid
  end
  
  def secret
    id_hash [ token, contact_id.to_s, birth_on.to_s, plz ].join
  end
  
  def first_sum_of_all
    Pref.app_value( :membership_entry_fee ) + 
        shares_volume +
        shares_volume * ( Pref.app_value( :share_buyers_fee ) / 100 )
  end
  
  def request_thumb
    picture_variant :thumb, gravity: 'Center', resize: '300x300^', crop: '300x300+0+0', quality: '40'
  end
  
  # --- from Picturize Concern
  def picture_variant( in_selector, in_options = {} )
    logger.debug "> picture_variant @ picturize - in_selector:#{in_selector} - in_options:#{in_options}"
    logger.debug "-- @_request_variant:#{@_request_variant}"
    unless defined? @_request_variant
      @_request_variant = {}
    end
    unless @_request_variant.has_key? in_selector
      @_request_variant[ in_selector ] = nil
      
      if has_request?
        begin
          @_request_variant[ in_selector ] = request.variant( combine_options: in_options )
          
        rescue
          # nothing
          logger.debug "-- had to be rescued..."
        end
      end
    end
    
    return @_request_variant[ in_selector ]
  end
  
  def transactions
    transactions = shares + payments
    transactions.sort_by { |t| t.sort_date }
  end
  
  # --- PREDICATES -----------------------------
  
  def application?
    status == 'new'
  end
  
  def accepted?
    status == 'accepted' and !discharged?
  end
  
  def member?
    accepted?
  end
  
  def discharged?
    ends_at and ends_at < Time.zone.now
  end
  
  def direct_debit?
    fee_mode == 'direct_debit'
  end
  
  def show_to?( in_status = 'public' )
    in_status == 'admin'
  end
  
  def has_request?
    request.attached?
  end
  
  def request_broken?
    request_thumb.nil?
  end
  
  # --- ACTIONS --------------------------------
  
  def paid_until_at
    
  end
  
  def advance_fee_check
    logger.debug "> advance_fee_check @ membership"
    last_payment = payments.first
    paid_for_months = ( last_payment.amount / ( fee_amount / fee_period ) )
    logger.debug "-- paid_for_months: #{paid_for_months}"
    first_of_month_at = Time.zone.now.at_beginning_of_month
    self.update_attribute :fee_check_at, first_of_month_at + ( paid_for_months * 30 ).days + 5.days + rand( 300 ).minutes
  end
  
  def send_fee_request
    logger.debug "> send_fee_request @ membership"
    self.update_attribute :fee_check_at, Time.zone.now + FEE_REMINDER_PERIOD + rand( 300 ).minutes
    MembershipsMailer.
        with( membership: self ).
        payment_request.
        deliver_later
  end
  
  def check_open_shares
    logger.debug "> check_open_shares @ membership"
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.t_period_select
    PERIOD_OPTIONS.collect { |o| [ self.human_attribute_name( "fee_period.#{o}" ), o ] }
  end
  
  def self.t_gender_select
    GENDER_OPTIONS.collect { |o| [ self.human_attribute_name( "gender.#{o}" ), o ] }
  end
  
  def self.t_mode_select
    MODE_OPTIONS.collect { |o| [ self.human_attribute_name( "fee_mode.#{o}" ), o ] }
  end
  
  def self.generate_membership_fees
    self.where( status: [ :member, :published ] ).inject( 0 ) { |sum, m|
        sum + m.fee_amount.to_f / m.fee_period.to_f }
  end
  
  def self.potential_membership_fees
    self.all.inject( 0 ) { |sum, m|
        sum + m.fee_amount.to_f / m.fee_period.to_f }
  end
  
  def self.proposed_share_volume
    self.where( status: [ :accepted, :member ] ).inject( 0 ) { |sum, m|
        sum + m.shares_volume }
  end
  
  def self.potential_share_volume
    self.all.inject( 0 ) { |sum, m|
        sum + m.shares_volume }
  end
  
  def self.with_saldo
    with_payments = self.includes( :shares, :payments ).where( 'payments.amount > 0' ).references( :shares, :payments )
    with_payments.select { |m|
        m.saldo_at( Time.zone.now ) != 0 }
  end
  
  def self.all_with_names
    self.all
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS = [
    'preliminary', # started membership online
    'completed', # completed membership online
    'confirmed', # confirmed online request
    'new', # new admin entry
    'accepted', # was accepted as member
    'cancelled', # was cancelled by admin or member
    'ended' # membership ended oder was deleted
  ]
  
  GENDER_OPTIONS = [
    'female',
    'male',
    'diverse'
  ]
  
  PERIOD_OPTIONS = [
    1,
    3,
    6,
    12
  ]
  
  MODE_OPTIONS = [
    'transferral',
    'paypal',
    'direct_debit'
  ]
  
  FEE_REMINDER_PERIOD = 8.days
  
end

