class Registration < ApplicationRecord
  include Tokenize
  
  after_initialize :fill_in_token
  
  belongs_to :event
  belongs_to :contact
  accepts_nested_attributes_for :contact
  
  # --- VALIDATIONS ----------------------------
  
  validates_presence_of :plz, :city, :emergency_nr
  validates_acceptance_of :accepts_storage
  
  # --- SCOPES ---------------------------------
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "reg_#{id}"
  end
  
  def to_param
    "#{token}"
  end
  
  def secret
    id_hash [ token, event_id.to_s, contact_id.to_s, plz ].join
  end
  
  # --- CONSTANTS ------------------------------
  
end
