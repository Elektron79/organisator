class Contact < ApplicationRecord
  include Checkerize
  include Confirmable
  include Tokenize
  after_initialize :fill_in_token
  
  has_one :account, dependent: :destroy
  has_one :membership, dependent: :destroy
  
  has_many :taggings, as: :entity, dependent: :destroy
  has_many :tags, -> { order( label: :asc ) }, as: :entity, through: :taggings
  
  has_many :mailings, dependent: :destroy
  has_many :received_messages, as: :receiver, class_name: 'Message', dependent: :destroy
  
  has_many :registrations, dependent: :destroy
  has_many :questions, dependent: :nullify
  has_many :answers, dependent: :nullify
  
  has_many :donations, dependent: :nullify
  
  has_many :participations, foreign_key: :participant, dependent: :destroy
  
  # --- VALIDATIONS ---------------------------
  
  validates :token,
        uniqueness: true,
        on: :create
  validates :token,
        presence: true
  validates :email, :first_name, :last_name,
        presence: true,
        on: :create,
        unless: :is_prospect?
  validates_acceptance_of :read_privacy, unless: :is_prospect?
  
  validate :check_question, on: :create, unless: :is_prospect?
  
  # --- SCOPES --------------------------------
  
  scope :new_all, -> { where status: 'new' }
  scope :fresh, -> { new_all.where 'updated_at < ?', Time.zone.now }
  scope :confirmed, -> {
        where status: 'confirmed' }
  scope :wants_news, -> {
        confirmed
        .where wants_news: true }
  scope :offers_help, -> {
        where offers_help: true }
  scope :new_since, -> (date) {
        where "created_at > ?", date }
  
  # --- INSTANCE METHODS ----------------------
  
  def to_param
    token
  end
  
  def name_text
    [ first_name, last_name ].join ' '
  end
  
  def preview_text
    name_text
  end
  
  def street_nr_text
    [ street, housenr ].compact.join ' '
  end
  
  def plz_city_text
    [ plz, city ].compact.join ' '
  end
  
  def secret
    id_hash [ first_name, last_name ].join
  end
  
  def quali_email
    '"' + name_text + '" <' + email + '>'
  end
  
  def streams
    participations.
        collect { |p|
            p.entity.is_a?( Stream ) ? p.entity : nil }.
        compact
  end
  
  # --- PREDICATES ----------------------------
  
  def is_prospect?
    status == 'prospect'
  end
  
  def is_member?
    !membership.blank? and membership.member?
  end
  
  def is_accepted?
    !membership.blank? and membership.accepted?
  end
  
  def is_applicant?
    !membership.blank? and membership.application?
  end
  
  def has_account?
    !account.blank?
  end
  
  def has_partial_address?
    !street.blank? or !housenr.blank? or !plz.blank? or !city.blank?
  end
  
  def show_to?( in_status = 'public' )
    return true if [ 'admin' ].include? in_status
    published?
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.new_since_days( in_days )
    self.new_since( Time.zone.now - in_days.days ).count
  end
  
  def self.increase_since_days( in_days )
    actual_days = self.
        new_since( Time.zone.now - in_days.days ).
        count
    prev_days = self.
        new_since( Time.zone.now - ( in_days * 2).days ).
        count - actual_days
    return ( actual_days * 100.0 / prev_days ).round - 100
  end
  
  def self.t_status_select
    STATUS_OPTIONS.collect { |o| [ self.human_attribute_name( "status.#{o}" ), o ] }
  end
  
private
  
  # --- CONSTANTS -----------------------------
  
  STATUS_OPTIONS = [
    'prospect',
    'new',
    'order',
    'membership',
    'confirmed'
  ]
  
end

