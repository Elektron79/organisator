class Tag < ApplicationRecord
  
  belongs_to :parent, class_name: 'Tag', optional: true
  has_many :children, foreign_key: :parent_id, class_name: 'Tag', dependent: :nullify
  has_many :taggings, dependent: :destroy
  
  # -------------------------------------------
  
  def preview_text
    label
  end
  
  def tagging_for( in_entity )
    taggings.detect { |t| t.entity == in_entity }
  end
  
  def entities
    taggings.collect { |t| t.entity }
  end
  
end
