class Account < ApplicationRecord
  include Confirmable
  include Tokenize
  include Picturize
  
  after_initialize :fill_in_token
  
  attr_accessor :old_password
  after_destroy :ensure_an_admin_remains
  
  has_secure_password
  
  # --- RELATIONS -----------------------------
  
  belongs_to :contact
  accepts_nested_attributes_for :contact
  has_one_attached :picture
  
  has_many :logs, -> { order created_at: :desc }, foreign_key: :actor_id
  
  has_many :articles, foreign_key: :author_id
  
  has_many :decisions
  has_many :proposals
  has_many :comments
  has_many :arguments
  has_many :votes, dependent: :destroy
  
  has_many :fundings
  has_many :owners, dependent: :destroy
  
  has_many :streams, foreign_key: :owner_id
  
  # activity tracking - created payments
  has_many :payments
  
  
  # --- VALIDATIONS ---------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates :login, presence: true,
        uniqueness: true
  validates_presence_of :password, on: :create
  validates_confirmation_of :password, on: :create
  validate :resetting_password_with_old
  
  # --- SCOPES --------------------------------
  
  scope :new_last_week, -> { where "created_at >= :start and created_at < :end", start: Time.zone.now.at_beginning_of_day.at_beginning_of_week - 7.days, end: Time.zone.now.at_beginning_of_day.at_beginning_of_week }
  scope :published, -> { all.reject { |a|
          !a.published? } }
  scope :confirmed, -> { all.reject { |a|
          !a.confirmed? } }
  scope :with_infos, -> { where "CHAR_LENGTH( label ) > 0 OR CHAR_LENGTH( displayname ) > 0 OR CHAR_LENGTH( description ) > 0" }
  
  # --- INSTANCE METHODS ----------------------
  
  def to_param
    token
  end
  
  def preview_text
    name_text
  end
  
  def name_text
    contact.name_text
  end
  def first_name
    contact.first_name
  end
  
  def displayname_text
    return displayname unless displayname.blank?
    contact.name_text
  end
  
  def membership
    contact.membership
  end
  
  def status_text
    case
      when ( status == 'confirmed' and has_membership? )
        'Mitglied'
      else
        status.capitalize
    end
  end
  
  def last_check
    return nil if logs.blank?
    
    #determine the last day with signifikant activity
    log_min_one_day_ago = logs.detect { |l| Time.zone.now.at_midnight - 1.day > l.created_at }
    log_min_one_day_ago ? log_min_one_day_ago.created_at : logs.last.created_at
  end
  
  def resetting_password_with_old
    unless password.blank?
      errors.add( :old_password, 'nicht korrekt' ) if old_password == 'wrong!'
      logger.debug "-- errors.full_messages: #{errors.full_messages}"
      #debugger
    end
  end
  
  def accounts_interacted
    # list of other accounts with which the
    # account has interacted with, ordered by
    # last interaction
    []
  end
  
  # --- PREDICATES ----------------------------
  
  def has_status?( in_status )
    STATUS_OPTIONS.find_index( in_status ) <= STATUS_OPTIONS.find_index( status )
  end
  
  def is_confirmed?
    confirmed? or is_team?
  end
  
  def published?
    is_team?
  end
  
  def is_internal?
    is_sales? or is_team? or is_member?
  end
  
  def accepted?
    status == 'accepted'
  end
  
  def is_admin?
    status == 'admin'
  end
  
  def is_sales?
    status == 'sales' or is_admin?
  end
  
  def is_editor?
    status == 'editor' or is_admin?
  end
  
  def is_team?
    status == 'team' or is_editor?
  end
  
  def is_member?
    is_confirmed? and has_membership?
  end
  
  def is_team_or_member?
    is_team? or is_member?
  end
  
  def has_membership?
    contact and contact.membership
  end
  
  def show_to?( in_status = 'public' )
    published?
  end
  
  def has_picture?
    picture.attached?
  end
  
  # --- Picture Variants ----------------------
  
  # --- Voting --------------------------------
  
  def can_vote_for?( in_proposal )
    in_proposal and in_proposal.votes.for_account( self ).all? { |v| v.status == 'new' }
  end
  
  # --- Mailings ------------------------------
  
  def send_password_reset
    if contact and contact.email.to_s.include?( '@' )
      logger.debug "> send_password_reset @ account -- id:#{id} email:#{contact.email}"
      
      self.reset_token = generate_token( 15 )
      self.reset_expires_at = Time.zone.now + 2.hours
      save!
      AccountsMailer.with( account: self ).password_reset_mail.deliver_later
    end
  end
  
  # --- Error Handling ------------------------
  
  class Error < StandardError
  end
  
  # --- CLASS METHODS -------------------------
  
  def self.only_one_team?
    self.where( status: %w( admin editor team ) ).size <= 1
  end
  
  def self.t_status_select
    STATUS_OPTIONS.collect { |o| [ self.human_attribute_name( "status.#{o}" ), o ] }
  end
  
  def self.all_status_below( in_status )
    STATUS_OPTIONS.first( STATUS_OPTIONS.find_index( in_status ) +1 )
  end
  
private
  
  def ensure_an_admin_remains
    if Account.count.zero?
      raise Error.new "Can't delete last user"
    end
  end
  
  # --- CONSTANTS -----------------------------
  
  STATUS_OPTIONS = [
    'new',
    'confirmed',
    'sales',
    'team',
    'editor',
    'admin'
  ]
  
end
