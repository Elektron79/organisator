class Event < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  include Publish
  after_initialize :set_publish_status
  
  attr_accessor :length
  
  before_save :calc_ends_at
  
  has_many :registrations
  
  has_many :taggings, as: :entity, dependent: :destroy
  has_many :tags, -> { order( label: :asc ) }, as: :entity, through: :taggings
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :title
  validates_length_of :title, within: 3..250
  validates_length_of :description, maximum: 250
  validates_length_of :location, maximum: 250
  validates_length_of :url, maximum: 250
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where status: 'published' }
  scope :upcoming, -> ( n ) {
        where( 'starts_at > ?', Time.zone.now ).
        order( 'starts_at ASC' )[ 0..(n-1) ] }
  scope :for_mail_from, -> ( from ) {
        where( 'starts_at > ?', from ).
        where( 'starts_at < ?', from + OUTLOOK_DAYS ).
        order 'starts_at ASC' }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}-#{title.parameterize}"
  end
  
  def preview_text
    title
  end
  
  def description_ical_text
    result = ''
    line = ''
    description.strip.split( ' ' ).each do |word|
      if line.length + word.length < 35
        if line.blank?
          line = word
        else
          line = line + ' ' + word
        end
      else
        if result.blank?
          result = line
        else
          result = result + "\r\n  " + line
        end
        line = word
      end
    end
    
    unless line.blank?
      if result.blank?
        result = line
      else
        result = result + "\r\n  " + line
      end
    end
    
    return result
  end
  
  def longer_than_normal
    starts_at.at_beginning_of_day != ends_at.at_beginning_of_day
  end
  
  def calc_ends_at
    self.ends_at = starts_at + length.to_i.hours
  end
  
  def starts_at_text
    text = I18n.l( starts_at, format: :html_break_long )
    text += '<br/>&mdash; ' + I18n.l( ends_at, format: :html_break_long ) if longer_than_normal
    return text.html_safe
  end
  
  def registration_enabled?
    !registration_starts_at.blank?
  end
  
  def remaining_registrations
    # if no max ist set infinite registrations
    # are possible
    return nil if max_registrations.blank?
    
    # otherwise count remaining number
    max_registrations.to_i - registrations.count
  end
  
  def sold_out?
    max_registrations.to_i > 0 and
        registrations.count >= max_registrations.to_i
  end
  
  def registration_not_started?
    !registration_starts_at.blank? and
    registration_starts_at > Time.zone.now
  end
  
  def registration_passed?
    !registration_ends_at.blank? and
    registration_ends_at < Time.zone.now
  end
  
  def registration_possible?
    # return whether reg still possible
    registration_enabled? and
        !registration_not_started? and
        !registration_passed? and
        ( remaining_registrations.blank? or
            remaining_registrations.to_i > 0 )
  end
  
  def show_to?( in_status = 'public' )
    return true if [ 'admin', 'editor' ].include? in_status
    published?
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.select_length
    LENGTH_VALUES
  end
  
  # --- CONSTANTS ------------------------------
  
  OUTLOOK_DAYS = 28.days
  
  LENGTH_VALUES = [
    [ "1 Stunde", 1 ],
    [ "2 Stunden", 2 ],
    [ "3 Stunden", 3 ],
    [ "4 Stunden", 4 ],
    [ "8 Stunden", 8 ],
    [ "1 Tag", 24 ],
    [ "2 Tage", 48 ],
    [ "3 Tage", 72 ],
    [ "4 Tage", 96 ],
    [ "5 Tage", 120 ],
    [ "1 Woche", 168 ] ]
  
end
