module Publish
  extend ActiveSupport::Concern
  
  # --- INSTANCE METHODS -----------------------
  
  def published?
    #logger.debug "-- self.status: #{self.status}"
    self.status == 'published'
  end
  
  def set_publish_status
    self.status ||= 'new'
  end
  
  # --- CONSTANTS ------------------------------
  
end
