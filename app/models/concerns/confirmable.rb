module Confirmable
  extend ActiveSupport::Concern
  
  # --- PREDICATES ----------------------------
  
  def confirmed?
    status == 'confirmed'
  end
  
  # --- INSTANCE METHODS ----------------------
  
  def confirm
    #logger.debug "-- confirm @ Confirmable : status:#{status}"
    self.status = 'confirmed' if LESSER_STATUS.include? status
    #logger.debug "-- LESSER_STATUS.include?: #{LESSER_STATUS.include? status} - status: #{status}"
  end
  
  # --- CONSTANTS -----------------------------
  
  LESSER_STATUS = %w( new prospect membership registration order )
  
end

