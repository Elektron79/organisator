class Payment < ApplicationRecord
  before_validation :lookup_membership
  
  belongs_to :membership
  belongs_to :creator, class_name: 'Account'
  
  # --- SCOPES ---------------------------------
  
  scope :up_to, -> ( in_date ) {
      where( 'paid_at < ?', in_date ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "py#{id}"
  end
  
  def membership_token
    membership ? membership.token : nil
  end
  def membership_token=( in_token )
    @_membership_token = in_token
  end
  
  def sort_date
    return paid_at if paid_at
    created_at if created_at
    Time.zone.now
  end
  
  def lookup_membership
    logger.debug "> lookup_membership @ payment"
    membership = Membership.find_by token: @_membership_token
    self.membership = membership
  end
  
end
