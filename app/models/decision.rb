class Decision < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  include Publish
  after_initialize :set_publish_status
  
  belongs_to :account
  has_many :proposals, dependent: :destroy
  
  # --- VALIDATIONS ---------------------------
  
  validates_presence_of :title
  
  # --- SCOPES --------------------------------
  
  scope :fresh, -> {
        where status: 'new' }
  scope :published, -> {
        where status: 'published' }
  scope :last_created, -> {
        order created_at: :desc }
  
  # --- PREDICATES ----------------------------
  
  def is_selection?
    variant == 'selection'
  end
  
  def preparing?
    !published?
  end
  
  def proposing?
    #logger.debug "-- published?: #{published?}"
    published? and Time.zone.now <= end_proposals_at
  end
  
  def voting?
    published? and ( Time.zone.now > end_proposals_at and
        Time.zone.now <= end_voting_at )
  end
  
  def votable_by?( in_account )
    voting? and not_voted_by?( in_account )
  end
  
  def active?
    published? and ( Time.zone.now <= end_voting_at )
  end
  
  def done?
    published? and ( Time.zone.now > end_voting_at )
  end
  
  def old?
    published? and ( Time.zone.now - OLD_TIME_SPAN > ( show_results_at.blank? ? end_voting_at : show_results_at ) )
  end
  
  def show_results?
    return done? if show_results_at.blank?
    published? and ( Time.zone.now > show_results_at )
  end
  
  def not_voted_by?( in_account )
    return true if in_account.blank?
    in_account.votes.for_decision( self ).all? { |v| v.status == 'new' }
  end
  
  def voted_by?( in_account )
    logger.debug "-- voted_by? in_account: #{in_account}"
    return false if in_account.blank?
    votes = proposals.collect { |p| p.votes_for in_account }.flatten.uniq
    return ( votes.count > 0 and votes.all? { |v|
              v.status == 'published' } )
  end
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}"
  end
  
  def preview_text
    title
  end
  
  def stage
    case
      when status == 'new'
        'preparation'
      when proposing?
        'proposing'
      when voting?
        'voting'
      when ( done? and !old? )
        'done'
      when ( done? and old? )
        'archive'
    end
  end
  
  def vote_count_for( in_account )
    proposals.collect { |p|
        p.vote_for in_account }.compact.count
  end
  
  def proposal_count
    proposals.published.count
  end
  
  def voters_published
    unless defined? @_voters_published
      @_voters_published = proposals.published.collect { |p|
        p.voters_published }.flatten.uniq
    end
    return @_voters_published
  end
  
  def winning_proposals
    return nil unless proposals.any? { |p| p.has_votes? }
    if is_selection?
      proposals.
          reject { |p| p.selection_value < 1 }.
          sort_by { |p| p.selection_value }.
          reverse[ 0..(vote_count - 1) ]
    else
      proposals.
          sort_by { |p| p.votes_value }.
          first
    end
  end
  
  def proposers_name_text
    proposals.
        collect { |p| p.account }.
        uniq.
        sort { |a,b| a.displayname_text <=> b.displayname_text }.
        collect { |a| a.displayname_text }.
        join ', '
  end
  
  # --- CLASS METHODS -------------------------
  
  def self.t_variant_select
    VARIANT_OPTIONS.collect { |o| [ self.human_attribute_name( "variant.#{o}" ), o ] }
  end
  
  # --- CONSTANTS -----------------------------
  
  OLD_TIME_SPAN = 3.weeks
  
  VARIANT_OPTIONS = [
    'sk',
    'selection'
  ]
  
end
