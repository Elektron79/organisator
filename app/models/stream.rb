class Stream < ApplicationRecord
  attr_accessor :participant_email
  
  belongs_to :owner, class_name: 'Account'
  has_many :participations, as: :entity
  
  # --- INSTANCE METHODS -----------------------
  
  def title_text
    return title unless title.blank?
    participants_name_list
  end
  
  def participants_name_list
    participations.collect { |p|
        p.participant.is_prospect? ? p.participant.email : p.participant.name_text }.join( ', ' )
  end
  
  def last_message_at
    Time.zone.now
  end
  
  def last_message_text
    '0 Nachrichten'
  end
  
  # --- PREDICATES -----------------------------
  
  def started?
    status == 'running'
  end
  
end
