class OrdersMailer < ApplicationMailer
  helper ApplicationHelper
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.orders_mailer.confirmation_mail.subject
  #
  def confirmation_mail
    @order = params[ :order ]
    @contact = @order.contact
    @confirmation_link  = activate_order_url( @order, ring: @order.secret )
    
    mail to: @contact.quali_email,
          subject: 'Bitte bestätigen... | ' + h_orga_name
  end
  
  def new_order_notification_mail
    @order = params[ :order ]
    @receiver = 'an die Bestellungsverwaltung'
    @login_link  = login_url
    
    notification_mail = h_app_value( 'order_notification_mail' )
    if notification_mail
      mail to: notification_mail,
          subject: 'Neue Bestellung(en) vorhanden | ' + h_orga_name
    end
  end
  
end
