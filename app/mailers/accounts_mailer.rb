class AccountsMailer < ApplicationMailer
  helper ApplicationHelper
  helper NewslettersHelper
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.accounts_mailer.password_reset_mail.subject
  #
  def password_reset_mail
    @account = params[ :account ]
    @password_reset_link  = reset_password_account_url( @account, to: @account.reset_token )
    
    mail to: @account.contact.email,
          subject: 'Kennwort vergessen?... | ' + h_orga_name
  end
  
  def comment_received_mail
    @comment = params[ :comment ]
    @account = @comment.entity.account
    @entity_url = url_for @comment.entity
    
    mail to: @account.contact.email,
        subject: 'Du hast einen neuen Kommentar erhalten | ' + h_orga_name
  end
  
end
