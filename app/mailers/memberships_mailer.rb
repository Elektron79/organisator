class MembershipsMailer < ApplicationMailer
  helper ApplicationHelper
  default from: '"💙 Mitglieder Betreuung ' + Rails.configuration.orga_name + '" <mitglieder@' + Rails.configuration.orga_domain + '>'
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.memberships_mailer.membership_accepted.subject
  #
  def membership_accepted
    @membership = params[ :membership ]
    @contact = @membership.contact
    @confirmation_link  = activate_membership_url( @membership, ring: @membership.secret )
    
    mail to: @contact.quali_email,
          subject: 'Herzlich Willkommen, liebes Mitglied! | ' + h_orga_name
  end
  
  def payment_request
    @membership = params[ :membership ]
    @contact = @membership.contact
    
    mail to: @contact.quali_email,
          subject: "Dein Mitgliedsbeitrag für #{l Time.zone.now, format: :month_year } | " + h_orga_name
  end
  
end
