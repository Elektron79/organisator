class ApplicationMailer < ActionMailer::Base
  include ApplicationHelper
  
  default from: '"Lisa Muster" <lisa@' + Rails.configuration.orga_domain + '>', reply_to: '"Lisa Muster" <lisa@' + Rails.configuration.orga_domain + '>'
  layout 'mailer'
  
  def h_app_feature?( in_feature )
    pref = Pref.find_by key: in_feature
    return ( pref and pref.active? )
  end
  helper_method :h_app_feature?
  
  def h_app_value( in_feature )
    pref = Pref.find_by key: in_feature
    return pref ? pref.value : nil
  end
  helper_method :h_app_value
  
end
