json.extract! argument, :id, :token, :status, :account_id, :proposal_id, :title, :description, :created_at, :updated_at
json.url argument_url(argument, format: :json)
