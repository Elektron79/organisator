json.extract! share, :id, :status, :owner_id, :count, :amount, :bought_at, :sold_at, :created_at, :updated_at
json.url share_url(share, format: :json)
