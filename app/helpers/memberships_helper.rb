module MembershipsHelper
  
  def h_fee_mode_class
    @membership.fee_mode != 'direct_debit' ? 'initially_hidden' : nil
  end
  
  def h_max_steps
    h_app_feature?( :allow_shares ) ? 5 : 4
  end
  
  def h_actor_for( in_membership, in_action )
    logs = @membership.logs.where( action: in_action )
    log = logs.last if in_action.include? 'create'
    log = logs.first if in_action.include? 'update' or in_action.include? 'accept'
    return '' unless log
    return "von #{log.actor.name_text}"
  end
  
end
