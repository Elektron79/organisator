module CommunityHelper
  
  def h_show_avatars?( in_accounts )
    h_avatar_accounts( in_accounts ).count > 0
  end
  
  def h_accounts_names( in_accounts )
    if h_show_avatars?( in_accounts )
      h_avatar_accounts( in_accounts ).
          collect { |a|
              a.displayname_text.
                  split( ' ' ).
                  first }.
                  join ', '
      
    elsif in_accounts.count > 0
      in_accounts[ 0..2 ].
          collect { |a|
              a.displayname_text.
                  split( ' ' ).
                  first }.
                  join ', '
    else
      'Niemand'
    end
  end
  
  def h_additional_count( in_accounts )
    in_accounts.count - h_accounts_names( in_accounts ).split( ', ' ).count
  end
  
  def h_avatar_accounts( in_accounts )
    in_accounts.
        select { |a|
            a.has_picture? }[ 0..2 ]
  end
  
  def h_save_plz?
    current_account and
        current_account.contact.plz.blank?
  end
  
end

