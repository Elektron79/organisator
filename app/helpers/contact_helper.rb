module ContactHelper
  
  def h_contact_editable?( in_contact )
    !current_account.blank? and
        ( in_contact == current_account.contact or
        current_account.is_admin? )
  end
  
end
