module AccountHelper
  
  def h_account_editable?( in_account )
    !current_account.blank? and
        in_account == current_account
  end
  
  def h_something_new?
    !@memberships.blank? or
        !@accounts.blank? or
        !@contacts.blank?
  end
  
end
