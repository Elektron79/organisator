module PaymentsHelper
  
  def h_payment_editable?( in_payment )
    h_is_admin? and
        in_payment and
        in_payment.updated_at > Time.zone.now - 1. day
  end
  
end
