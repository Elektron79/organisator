module ApplicationHelper
  
  # --- Orga Specififcs ------------------------
  
  def h_orga_name
    Rails.configuration.orga_name
  end
  
  def h_orga_file_name
    h_orga_name.gsub( ' ', '_' )
        .gsub( 'ä', 'ae' )
        .gsub( 'ö', 'oe' )
        .gsub( 'ü', 'ue' )
        .gsub( 'ß', 'ss' )
        .gsub( '*', '_' )
        .gsub( ':', '_' )
  end
  
  def h_orga_domain
    Rails.configuration.orga_domain
  end
  
  def h_orga_slogan
    Rails.configuration.orga_slogan
  end
  
  def h_orga_contact_reply_mail
    pref = Pref.where( key: 'contact_reply_mail' )
    return 'lisa.muster@organisator.org' if pref.blank? or pref.first.blank? or pref.first.value.blank?
    return pref.first.value
  end
  
  def h_orga_color
    Rails.configuration.orga_color
  end
  
  # --- Role & Menu ---------------------------
  
  def h_show_menu?( in_key )
    # show only if a first entity is
    # in the database, otherwise only
    # accessible via role overview page
    klass = in_key.to_s.classify.constantize
    return klass.count > 0
  end
  
  def h_menu_button_klass
    klass = 'menu_button'
    klass += ' with_text' if h_app_value :menu_text
    return klass
  end
  
  def h_dont_show_login( in_page )
    current_account and in_page.path == 'login'
  end
  
  # --- Specififcs -----------------------------
  
  def h_badge_membership_title( in_contact )
    case
      when in_contact.membership.member?
        'Mitgliedschaft'
      when in_contact.membership.accepted?
        'Antrag angenommen'
      else
        'Antrag'
    end
  end
  
  # --- TEXT FORMATTING ------------------------
  
  def h_format( in_text = '' )
    return '' if in_text.blank?
    auto_link( sanitize( in_text.strip.gsub( "\n", '<br />' ), tags: %w(br a strong em div) ) ).html_safe
  end
  
  def h_format_preview( in_text = '', in_length )
    truncate strip_tags( in_text ), length: in_length
  end
  
  def h_transform_hs( in_text = "" )
    
  end
  
  # --- PAGE TITLE & DECRIPTION ----------------
  
  def h_page_title_for( in_path = '' )
    page = Page.
        where( path: in_path ).
        first
    page.blank? ? nil : page.title
  end
  
  def h_page_description_for( in_path = '' )
    page = Page.
        where( path: in_path ).
        first
    page.blank? ? nil : page.description
  end
  
  # --- OpenGraph Definitionen -----------------
  
  def h_og_type
    @og_type || @og_image ? 'summary_large_image' : 'summary'
  end
  
  def h_og_title
    @og_title || @page_title || h_orga_name
  end
  
  def h_og_description
    @og_description || h_orga_name + ' ist eine tolle Organisation, die...'
  end
  
  def h_og_image
    @og_image || image_url( "logos/#{h_orga_file_name}_Logo_q512.png" )
  end
  
  def h_tw_url
    request.original_url
  end
  # --- setzen
  
  def h_page_klass( in_klass = nil )
    if in_klass
      @_page_klass = in_klass
    end
    
    page_klass = 'page_'
    page_klass += params[ :controller ].parameterize separator: '_'
    page_klass += '_' + params[ :action ]
    
    page_klass += ' ' + @_page_klass if defined? @_page_klass
    return page_klass
  end
  
  def h_page_klass_color
    case @_page_klass
      when 'headerless_page'
        # use $back_col
        '#f0e9de'
      when 'important_page'
        # use $back_col
        '#0071bc'
      else
        # use $header_col
        '#0071bc'
    end
  end
  
  def h_page_title( in_title = '' )
    unless in_title.blank?
      @page_title = in_title
      
    else
      return h_orga_name if @page_title.blank?
      return @page_title + ' @ ' + h_orga_name
    end
  end
  
  def h_type( in_type )
    @og_type = in_type
  end
  
  def h_title( in_title )
    @og_title = in_title
  end
  
  def h_description( in_description )
    @og_description = in_description
  end
  
  def h_image( in_image )
    @og_image = in_image
  end
  
end
