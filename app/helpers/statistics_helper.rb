module StatisticsHelper
  
  def h_quot in_a, in_b
    ( 1.0 * in_a / in_b * 100 ).round - 100 if in_b > 0
  end
  
  def h_quot_class( in_a, in_b )
    quot = h_quot in_a, in_b
    value_class = quot.to_f > 0 ?
        'positive' : ( quot.to_f < 0 ?
            'negative' : 'zero' )
    return "number percentage #{value_class}"
  end
  
end
