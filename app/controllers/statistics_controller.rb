class StatisticsController < ApplicationController
  
  # --- CUSTOM METHODS ------------------------
  
  def current_requests
    redirect_to login_path, alert: 'Um diese Seite zu sehen, musst du bestimmte Zugriffsrechte haben.' and return unless h_is_editor? or h_app_value( :team_sees_statistics )
    
    logger.debug "-- Time.zone.now: #{Time.zone.now}"
    @today = Time.zone.now.at_beginning_of_day
    logger.debug "-- @today: #{@today}"
    @this_week = @today.at_beginning_of_week
    logger.debug "-- @this_week: #{@this_week}"
    
    @requests_d0 = Log.requests_between @today, @today + 24.hours
    @requests_d1 = Log.requests_between @today - 24.hours, @today
    @requests_d2 = Log.requests_between @today - 48.hours, @today - 24.hours
    @requests_d3 = Log.requests_between @today - 72.hours, @today - 48.hours
    @requests_d4 = Log.requests_between @today - 96.hours, @today - 72.hours
    
    @requests_w0 = Log.requests_between @this_week, @this_week + 1.week
    @requests_w1 = Log.requests_between @this_week - 1.week, @this_week
    @requests_w2 = Log.requests_between @this_week - 2.weeks, @this_week - 1.week
    @requests_w3 = Log.requests_between @this_week - 3.weeks, @this_week - 2.week
    @requests_w4 = Log.requests_between @this_week - 4.weeks, @this_week - 3.week
    logger.debug "-- d0:#{@requests_d0.size}"
    logger.debug "-- d1:#{@requests_d1.size}"
    logger.debug "-- d2:#{@requests_d2.size}"
    logger.debug "-- w0:#{@requests_w0.size}"
    logger.debug "-- w1:#{@requests_w1.size}"
    logger.debug "-- w2:#{@requests_w2.size}"
    
    @entities = @requests_d0.
        collect { |r| r.entity }
    @entities |= @requests_d1.collect { |r| r.entity }
    @entities |= @requests_d2.collect { |r| r.entity }
    @entities |= @requests_d3.collect { |r| r.entity }
    @entities |= @requests_d4.collect { |r| r.entity }
    @entities |= @requests_w0.collect { |r| r.entity }
    @entities |= @requests_w1.collect { |r| r.entity }
    @entities |= @requests_w2.collect { |r| r.entity }
    @entities |= @requests_w3.collect { |r| r.entity }
    @entities |= @requests_w4.collect { |r| r.entity }
    @entities.uniq!
    @entities.compact!
    logger.debug "-- @entities.size:#{@entities.size}"
    
    @requests = @entities.collect { |e| [
          @requests_d0.detect { |r| r.entity == e },
          @requests_d1.detect { |r| r.entity == e },
          @requests_d2.detect { |r| r.entity == e },
          @requests_d3.detect { |r| r.entity == e },
          @requests_d4.detect { |r| r.entity == e },
          @requests_w0.detect { |r| r.entity == e },
          @requests_w1.detect { |r| r.entity == e },
          @requests_w2.detect { |r| r.entity == e },
          @requests_w3.detect { |r| r.entity == e },
          @requests_w4.detect { |r| r.entity == e } ] }
    #debugger
    @requests.sort! { |a,b|
        b.max_by { |r|
            r.blank? ? 0 : r.amount }.amount <=>
        a.max_by { |r|
            r.blank? ? 0 : r.amount }.amount }
  end
  
  def old_requests
    @today = Time.zone.now
    
    @requests_1 = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 24.hours ]
    @requests_2 = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 48.hours, t2: @today - 24.hours ]
    
    @requests_3 = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 72.hours, t2: @today - 48.hours ]
    
    @requests_w = Log.find_by_sql [ 'SELECT count(*) AS amount, entity_type, entity_id
        FROM logs
        WHERE created_at > :t1 AND created_at <= :t2
        GROUP BY entity_type, entity_id
        ORDER by amount DESC', t1: @today - 72.hours - 1.week, t2: @today - 72.hours ]
    
    @entities = @requests_1.
        collect { |r| r.entity }.
        compact
    @entities.reject! { |e| e.kind_of? Contact }
    d0 = @requests_1.detect { |r| r.entity == @entities.first }
    d1 = @requests_2.detect { |r| r.entity == @entities.first }
    d2 = @requests_3.detect { |r| r.entity == @entities.first }
    w = @requests_w.detect { |r| r.entity == @entities.first }
    @requests = @entities.collect { |e| [
          @requests_1.detect { |r| r.entity == e },
          @requests_2.detect { |r| r.entity == e },
          @requests_3.detect { |r| r.entity == e },
          @requests_w.detect { |r| r.entity == e } ].compact }
  end
  
  # --- STANDARD CRUD METHODS -----------------
  
  def index
  end
  
end
