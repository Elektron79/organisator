class ContentpartsController < ApplicationController
  before_action :set_contentpart, only: [ :to_top, :up, :down, :to_bottom, :show, :edit, :update, :destroy ]
  skip_before_action :require_team, only: [ :create, :update ]
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /contentparts/1/to_top
  def to_top
    @contentpart.move_to_top
    @contentpart.save
    
    redirect_to @contentpart.container
  end
  
  # PUT /contentparts/1/up
  def up
    @contentpart.move_higher
    @contentpart.save
    
    redirect_to @contentpart.container
  end
  
  # PUT /contentparts/1/down
  def down
    @contentpart.move_lower
    @contentpart.save
    
    redirect_to @contentpart.container
  end
  
  # PUT /contentparts/1/to_bottom
  def to_bottom
    @contentpart.move_to_bottom
    @contentpart.save
    
    redirect_to @contentpart.container
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /contentparts
  # GET /contentparts.json
  def index
    @contentparts = Contentpart.all
  end
  
  # GET /contentparts/1
  # GET /contentparts/1.json
  def show
  end
  
  # GET /contentparts/new
  def new
    @contentpart = Contentpart.new
  end
  
  # GET /contentparts/1/edit
  def edit
  end
  
  # POST /contentparts
  # POST /contentparts.json
  def create
    @contentpart = Contentpart.new( contentpart_params )
    
    respond_to do |format|
      if @contentpart.save
        format.html { redirect_to @contentpart.container, notice: 'Contentpart was successfully created.' }
        format.json { render :show, status: :created, location: @contentpart }
      else
        format.html { render :new }
        format.json { render json: @contentpart.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /contentparts/1
  # PATCH/PUT /contentparts/1.json
  def update
    respond_to do |format|
      if @contentpart.update( contentpart_params )
        format.html { redirect_to @contentpart.container, notice: 'Contentpart was successfully updated.' }
        format.json { render :show, status: :ok, location: @contentpart }
      else
        format.html { render :edit }
        format.json { render json: @contentpart.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /contentparts/1
  # DELETE /contentparts/1.json
  def destroy
    container = @contentpart.container
    @contentpart.destroy
    
    respond_to do |format|
      format.html { redirect_to container, notice: 'Contentpart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_contentpart
    @contentpart = Contentpart.find_by token: params[ :id ]
    render_404 and return if @contentpart.blank?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def contentpart_params
    params.require( :contentpart ).permit( :kind, :position, :container_id, :container_type, :content, :picture )
  end
  
end
