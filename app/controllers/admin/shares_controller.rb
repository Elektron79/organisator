class Admin::SharesController < ApplicationController
  skip_before_action :require_team
  before_action :require_admin
  
  before_action :set_membership, only: %i{ index new create }
  before_action :set_share, only: %i{ allocate show edit update destroy }
  
  # --- Custom Methods ------------------------
  
  # PUT /shares/1/allocate
  def allocate
    @share.update_attribute :bought_at, Time.zone.now
    redirect_to admin_membership_shares_path @share.membership
  end
  
  # --- Standard CRUD Methods -----------------
  
  # GET /shares or /shares.json
  def index
    @dates_list = true
    logger.debug "-- index @ admin/shares_controller"
    #logger.debug "-- @membership: #{@membership.to_yaml}"
    
    if @membership
      @transactions = @membership.shares + @membership.payments
      @transactions.sort_by! { |t| t.sort_date }
      
      render 'index_membership'
      
    else
      @shares = Share.all.to_ary.sort { |a,b|
            b.sort_date <=> a.sort_date }
      @shares_sum = Share.sum :count
      
      render 'index'
    end
    
    write_log 'index_shares'
  end
  
  # GET /shares/1 or /shares/1.json
  def show
  end
  
  # GET /shares/new
  def new
    @share = @membership.shares.build
  end
  
  # GET /shares/1/edit
  def edit
  end
  
  # POST /shares or /shares.json
  def create
    @share = @membership.shares.build( share_params )
    
    respond_to do |format|
      if @share.save
        format.html { redirect_to admin_membership_shares_path( @membership ), notice: "Share was successfully created." }
        format.json { render :show, status: :created, location: @share }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @share.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /shares/1 or /shares/1.json
  def update
    respond_to do |format|
      if @share.update(share_params)
        format.html { redirect_to share_url(@share), notice: "Share was successfully updated." }
        format.json { render :show, status: :ok, location: @share }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @share.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /shares/1 or /shares/1.json
  def destroy
    @share.destroy
    
    respond_to do |format|
      format.html { redirect_to shares_url, notice: "Share was successfully destroyed." }
      format.json { head :no_content }
    end
  end
  

private # -------------------------------------
  
  # Use callbacks to share common setup or constraints between actions.
  def set_share
    @share = Share.find(params[:id])
  end
  
  def set_membership
    #logger.debug "-- set_membership @ admin/shares_controller"
    @membership = Membership.find_by token: params[ :membership_id ]
  end
  
  # Only allow a list of trusted parameters through.
  def share_params
    params.require(:share).permit( :count, :amount, :ordered_at )
  end
  
end
