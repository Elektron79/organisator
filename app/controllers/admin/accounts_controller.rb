class Admin::AccountsController < ApplicationController
  include AccountHelper
  
  before_action :require_admin
  before_action :set_account, only: %i( charm show edit update destroy )
  
  # --- CUSTOM METHODS ------------------------
  
  # PATCH /admin/accounts/1/charm
  def charm
    reset_session
    set_current_account @account
    redirect_to mine_accounts_path( @account ), notice: 'Hokus Pokus! Du bist jetzt eine andere Person'
  end
  
  # --- STANDARD CRUD METHODS -----------------
  
  # GET /admin/accounts
  # GET /admin/accounts.json
  def index
    @status_selected = params[ :status ]
    if @status_selected.blank?
      @accounts = Account.order 'status, updated_at DESC'
    else
      @accounts = Account.where status: @status_selected
    end
  end
  
  # GET /admin/accounts/1
  # GET /admin/accounts/1.json
  def show
  end
  
  # GET /admin/accounts/new
  def new
    @account = Account.new( contact: Contact.new )
    unless params[ :email ].blank?
      @account.contact.email = params[ :email ]
      @account.login = params[ :email ]
    end
  end
  
  # GET /admin/accounts/1/edit
  def edit
  end
  
  # POST /admin/accounts
  # POST /admin/accounts.json
  def create
    @account = Account.new( account_params )
    @account.status = 'new'
    @account.contact.status = 'new' if @account.contact
    
    respond_to do |format|
      if @account.save
        ContactsMailer.with( contact: @account.contact ).
            confirmation_mail.
            deliver_later
        
        set_current_account @account
        
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /admin/accounts/1
  # PATCH/PUT /admin/accounts/1.json
  def update
    respond_to do |format|
      if @account.update( account_params )
        format.html { redirect_to admin_account_path( @account ), notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /admin/accounts/1
  # DELETE /admin/accounts/1.json
  def destroy
    write_log( 'destroy account', @account.to_yaml, @account )
    
    @contact = @account.contact
    @account.destroy
    respond_to do |format|
      format.html { redirect_to @contact, notice: 'Account wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find_by token: params[ :id ]
    render_404 and return if @account.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require( :account ).permit( :status, :login, :password, :password_confirmation, :displayname, :label, :description, :picture, :contact_attributes => [ :email, :first_name, :last_name, :is_member, :read_basics, :read_privacy, :wants_news ] )
  end
  
end
