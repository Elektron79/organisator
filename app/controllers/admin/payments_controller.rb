class Admin::PaymentsController < ApplicationController
  include PaymentsHelper
  
  before_action :require_admin
  
  before_action :check_enabled
  before_action :set_payment, only: [ :show, :edit, :update, :destroy ]
  before_action :set_membership, only: %i( new )
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.includes( membership: [ :contact ] ).references( membership: [ :contact ] ).limit( 100 ).order 'paid_at DESC'
    @new_payment = Payment.new
  end
  
  # GET /payments/new
  def new
    @payment = @membership.payments.build
  end
  
  # GET /payments/1/edit
  def edit
  end
  
  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new( payment_params )
    @payment.status = 'new'
    @payment.creator = current_account
    
    respond_to do |format|
      if @payment.save
        if h_app_feature? :allow_membership_fees
          @payment.membership.advance_fee_check
        end
        if h_app_feature? :allow_shares
          @payment.membership.check_open_shares
        end
        
        format.html { redirect_to admin_payments_path, notice: 'Zahlung wurde eingetragen.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update( payment_params )
        format.html { redirect_to admin_payments_path, notice: 'Zahlung wurde erfolgreich geändert.' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    write_log( 'destroy payment', @payment.to_yaml, @payment )
    
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to admin_payments_path, notice: 'Zahlung wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def check_enabled
    render :not_enabled and return unless Pref.find_by( key: :allow_payment ).value == 'true'
  end
  
  def set_payment
    @payment = Payment.find params[ :id ]
    render_404 and return if @payment.blank?
  end
  
  def set_membership
    @membership = Membership.find_by token: params[ :membership_id ]
    render_404 and return if @membership.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def payment_params
    params.require( :payment ).permit( :status, 
    :membership_token, :amount, :paid_at )
  end
  
end
