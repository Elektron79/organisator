class StreamsController < ApplicationController
  before_action :set_stream, only: %i{ show edit update destroy }
  
  # GET /streams or /streams.json
  def index
    @public_streams = Stream.where public:  true
    logger.debug "@public_streams: #{@public_streams.to_yaml}"
    
    @personal_streams = Stream.where owner: current_account
    logger.debug "@personal_streams: #{@personal_streams.to_yaml}"
    
    @private_streams = current_account.contact.streams
    logger.debug "@personal_streams: #{@personal_streams.to_yaml}"
    
    @my_streams = @personal_streams | @private_streams
  end
  
  # GET /streams/1 or /streams/1.json
  def show
    unless @stream.started?
      redirect_to stream_participations_path( @stream )
    else
      render :show
    end
  end
  
  # GET /streams/new
  def new
    @stream = Stream.new
  end
  
  # GET /streams/1/edit
  def edit
  end
  
  # POST /streams or /streams.json
  def create
    @stream = Stream.new( stream_params )
    @stream.status = 'new'
    @stream.owner = current_account
    
    @contact = Contact.find_by email: @stream.participant_email
    unless @contact
      @contact = Contact.create email: @stream.participant_email, status: 'prospect'
    end
    @stream.participations.build participant: @contact, status: 'new'
    
    respond_to do |format|
      if @stream.save
        format.html { redirect_to @stream, notice: "Chat wurde erfolgreich gesichert." }
        format.json { render :show, status: :created, location: @stream }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @stream.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /streams/1 or /streams/1.json
  def update
    stream_status = @stream.status
    
    respond_to do |format|
      if @stream.update( stream_params )
        # check wether status changed for
        # invitation mail
        if stream_status == 'new' and @stream.status == 'running'
          @stream.participations.each { |p|
              ContactsMailer.with( participation: p, stream: @stream ).stream_invitation_mail.deliver_later }
        end
        
        format.html { redirect_to @stream, notice: "Chat wurde erfolgreich geändert." }
        format.json { render :show, status: :ok, location: @stream }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @stream.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /streams/1 or /streams/1.json
  def destroy
    @stream.destroy
    respond_to do |format|
      format.html { redirect_to streams_url, notice: "Chat wurde erfolgreich gelöscht." }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_stream
    @stream = Stream.find( params[ :id ] )
  end
  
  # Only allow a list of trusted parameters through.
  def stream_params
    params.require( :stream ).permit( :token, :status, :owner_id, :public, :max_participants, :only_invites, :status_required, :fade_away_days, :participant_email  )
  end
  
end
