class TagsController < ApplicationController
  before_action :set_tag, only: %i[ orphanize show edit update destroy ]
  
  skip_before_action :require_team, only: %i[ quiz_categories ]
  
  # --- CUSTOM METHODS ------------------------
  
  # PUT /tags/1/add
  def add
    logger.debug "-- add @ tags controller"
    logger.debug params.to_yaml
  end
  
  # PUT /tags/1/orphanize
  def orphanize
    parent = @tag.parent
    
    if @tag.update_attribute 'parent', nil
      redirect_to parent, notice: 'Marke wurde erfolgreich entfernt.'
      write_log 'orphanize_tag', nil, @tag
      
    else
      redirect_to parent, alert: 'Marke konnte nicht entfernt werden.'
    end
  end
  
  # --- STANDARD CRUD METHODS -----------------
  
  # GET /tags
  # GET /tags.json
  def index
    redirect_to login_path, alert: 'Um diese Seite zu sehen, musst du bestimmte Zugriffsrechte haben.' and return unless h_is_editor? or h_app_value( :team_sees_tags )
    
    logger.debug "-- index @ tags controller"
    @tags = Tag.includes( :taggings ).order( 'kind ASC, label ASC' )
    
    write_log 'index_tags', current_account ? current_account.status : nil
  end
  
  # GET /tags/1
  # GET /tags/1.json
  def show
    write_log 'show_tag', nil, @tag
  end
  
  # POST /tags
  # POST /tags.json
  def create
    @tag = Tag.new( tag_params )
    @tag.status = 'new'
    
    if params[ :entity_type ]
      # should the new tag be attached to an
      # entity given?
      @entity = params[ :entity_type ].to_s.classify.constantize.find params[ :entity_id ]
      @tag.taggings.build( entity_type: @entity.class, entity_id: @entity.id )
      redirect_target = @entity
    else
      # create a new sub tag to an existing tag
      redirect_target = @tag.parent
    end
    
    respond_to do |format|
      if @tag.save
        format.html { redirect_to redirect_target, notice: 'Marke wurde erfolgreich angelegt und angehängt.' }
        format.json { render :show, status: :created, location: @tag }
        
        write_log 'create_tag', nil, @tag
      else
        format.html { render :new }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_is_editor?
    
    respond_to do |format|
      if @tag.update( tag_params )
        if tag_params[ :parent_id ]
          format.html { redirect_to @tag.parent, notice: 'Marke wurde erfolgreich geändert.' }
        else
          format.html { redirect_to @tag, notice: 'Marke wurde erfolgreich geändert.' }
        end
        format.json { render :show, status: :ok, location: @tag }
        
        write_log 'create_tag', nil, @tag
      else
        format.html { render :edit }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_tag
    @tag = Tag.find params[ :id ]
    render_404 and return if @tag.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def tag_params
    params.require( :tag ).permit( :label, :color, :parent_id )
  end
  
end
