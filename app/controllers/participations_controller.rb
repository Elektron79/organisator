class ParticipationsController < ApplicationController
  before_action :set_stream, only: %i{ index create }
  before_action :set_participation, only: %i{ show edit update destroy }
  
  # GET /participations or /participations.json
  def index
  end
  
  # GET /participations/1 or /participations/1.json
  def show
    redirect_to stream_participations_path( @stream )
  end
  
  # GET /participations/new
  def new
    @participation = @stream.participations.build
  end
  
  # GET /participations/1/edit
  def edit
  end
  
  # POST /participations or /participations.json
  def create
    @participation = @stream.participations.build( participation_params )
    @participation.status = 'new'
    
    @contact = Contact.find_by email: @participation.participant_email
    unless @contact
      @contact = Contact.create email: @participation.participant_email, status: 'prospect'
    end
    @participation.participant = @contact
    
    respond_to do |format|
      if @participation.save
        format.html { redirect_to stream_participations_path( @stream ), notice: "Teilnehmer*in wurde erfolgreich hinzugefügt." }
        format.json { render :show, status: :created, location: @participation }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @participation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /participations/1 or /participations/1.json
  def update
    respond_to do |format|
      if @participation.update( participation_params )
        format.html { redirect_to @participation, notice: "Chat wurde erfolgreich geändert." }
        format.json { render :show, status: :ok, location: @participation }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @participation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /participations/1 or /participations/1.json
  def destroy
    @stream = @participation.entity
    @participation.destroy
    
    respond_to do |format|
      format.html { redirect_to stream_participations_path( @stream ), notice: "Teilnehmer*in wurde erfolgreich gelöscht." }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_stream
    @stream = Stream.find( params[ :stream_id ] )
  end
  
  def set_participation
    @participation = Participation.find( params[ :id ] )
  end
  
  # Only allow a list of trusted parameters through.
  def participation_params
    params.require( :participation ).permit( :token, :status, :owner_id, :public, :max_participants, :only_invites, :status_required, :fade_away_days, :participant_email  )
  end
  
end
