class FundingsController < ApplicationController
  skip_before_action :require_team
  before_action :require_editor, except: [ :index, :show ]
  
  before_action :check_enabled
  before_action :set_funding, only: [ :show, :edit, :update, :destroy ]
  
  
  # --------------------------------------------
  # CRUD methods
  
  # GET /fundings
  # GET /fundings.json
  def index
    render 'pages/no_access' and return unless h_page_accessible? 'fundings', current_account
    
    if h_is_editor?
      @fundings = Funding.all
      
    else
      @fundings = Funding.published
    end
    @fundings.sort_by { |f|
          ( f.ends_at - Time.zone.now ).days }
    
    write_log 'index_fundings'
  end
  
  # GET /fundings/1
  # GET /fundings/1.json
  def show
    require_team_or_member unless @funding.published?
    
    write_log 'show_funding', nil, @funding
  end
  
  # GET /fundings/new
  def new
    @funding = Funding.new starts_at: Time.zone.now + 3.days, ends_at: Time.zone.now + 3.days + 1.month, goal_1: 100
  end
  
  # GET /fundings/1/edit
  def edit
  end
  
  # POST /fundings
  # POST /fundings.json
  def create
    @funding = Funding.new( funding_params  )
    @funding.status = 'new'
    @funding.account = current_account
    
    respond_to do |format|
      if @funding.save
        # create standard questions
        create_default_questions
        
        format.html { redirect_to fundings_path, notice: 'Funding was successfully created.' }
        format.json { render :show, status: :created, location: @funding }
      else
        format.html { render :new }
        format.json { render json: @funding.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /fundings/1
  # PATCH/PUT /fundings/1.json
  def update
    respond_to do |format|
      if @funding.update( funding_params )
        format.html { redirect_to @funding, notice: 'Funding was successfully updated.' }
        format.json { render :show, status: :ok, location: @funding }
      else
        format.html { render :edit }
        format.json { render json: @funding.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /fundings/1
  # DELETE /fundings/1.json
  def destroy
    @funding.destroy
    respond_to do |format|
      format.html { redirect_to fundings_url, notice: 'Finanzierung wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def check_enabled
    render 'pages/no_access' and return unless h_app_feature? :allow_funding
  end
  
  def set_funding
    @funding = Funding.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @funding.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def funding_params
    params.require( :funding ).permit( :status, :title, :slogan, :picture, :short_text, :goal_1, :goal_1_use, :goal_2, :goal_2_use, :goal_3, :goal_3_use, :starts_at, :ends_at,:performance_period, :description, :category, :location, :url, :some_1, :some_2, :some_3 )
  end
  
  def create_default_questions
    DEFAULT_QUESTIONS.each do |question|
      @funding.questions.create status: 'new', contact: current_account.contact, openness: 'closed', content: question
    end
  end
  
  # --- CONSTANTS ------------------------------
  
  DEFAULT_QUESTIONS = [
    'Worum geht es in diesem Projekt?',
    'Was sind die Ziele und wer ist die Zielgruppe?',
    'Warum sollte jemand dieses Projekt unterstützen?',
    'Was passiert mit dem Geld bei erfolgreicher Finanzierung?',
    'Wer steht hinter dem Projekt?'
  ]
  
end
