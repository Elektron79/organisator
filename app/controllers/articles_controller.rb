class ArticlesController < ApplicationController
  before_action :set_article, only: [ :publish, :show, :edit, :update, :destroy ]
  before_action :set_parent, only: [ :new, :index ]
  
  skip_before_action :require_team
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /articles/1/publish
  def publish
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless ( ( h_is_team? and current_account != @article.author ) or Account.only_one_team? )
    
    @article.status = 'published'
    
    respond_to do |format|
      if @article.save
        format.html { redirect_to articles_path, notice: 'Article was successfully published.' }
        format.json { render :show, status: :ok, location: @article }
      else
        flash.alert = 'Did not work!'
        format.html { render :show }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'publish_article', nil, @article
  end
  
  # --- CRUD METHODS ---------------------------
  
  # GET /articles
  # GET /articles.json
  def index
    @page = ( params[ :page ] || 0 ).to_i
    
    unless h_is_team?
      # outside access -----
      # 404 if the page is not accessible from
      # the outside
      render_404 and return if not_accessible?
      
      if defined? @parent
        @articles = @parent.articles.published
        
      else
        @articles = Article.published.general
        # add articles of one self
        # ?? @articles << Article.where( author: current_account ) if current_account
      end
      
    else
      # logged in as team member access -----
      if defined? @parent
        @articles = @parent.articles
        
      else
        if params[ :selection ] == 'candidatures'
          @articles = Article.with_parent
          
        else
          @articles = Article.general
        end
      end
    end
    
    @more_pages = @articles.size > ( @page + 1 ) * Article::PAGE_SIZE
    @articles = @articles.page( @page )
    
    write_log 'index_articles', current_account ? current_account.status : nil
  end
  
  # GET /articles/1
  # GET /articles/1.json
  def show
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless @article.published? or current_account == @article.author or h_is_team?
    
    write_log 'show_article', nil, @article
  end
  
  # GET /articles/new
  def new
    if defined? @parent
      @article = @parent.articles.build
    else
      @article = Article.new
    end
  end
  
  # GET /articles/1/edit
  def edit
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless current_account == @article.author or h_is_editor?
    
    write_log 'publish_article', nil, @article
  end
  
  # POST /articles
  # POST /articles.json
  def create
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless h_is_user?
    
    @article = Article.new( article_params )
    @article.status = 'new'
    @article.author = current_account
    
    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'create_article', nil, @article
  end
  
  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless current_account == @article.author or h_is_editor?
    
    respond_to do |format|
      if @article.update( article_params )
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'update_article', nil, @article
  end
  
  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless current_account == @article.author or h_is_editor?
    
    write_log 'destroy_article', nil, @article
    @article.destroy
    
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  def not_accessible?
    articles_page = Page.find_by path: 'articles'
    return ( articles_page.blank? or articles_page.private? )
  end
  
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @article.blank?
  end
  
  def set_parent
    if !params[ :parent_type ].blank?
      try_parent = params[ :parent_type ].constantize.find params[ :parent_id ]
      @parent = try_parent unless try_parent.blank?
    end
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def article_params
    params.require( :article ).permit( :title, :parent_type, :parent_id, :published_at, :description )
  end
  
end
