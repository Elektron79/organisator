class AccountsController < ApplicationController
  include AccountHelper
  
  skip_before_action :require_team, only: %i[ check request_password reset_password mine build index show new edit create update ]
  before_action :set_account, only: %i[ reset_password show edit update destroy ]
  
  # --- CUSTOM METHODS ------------------------
  
  # GET  /accounts/check
  # POST /accounts/check
  def check
    if request.post?
      @contact = Contact.find_by email: params[ :email ]
      
      if @contact.blank?
        # no contact exists, redirect
        redirect_to new_account_path( email: params[ :email ] ) and return
        
      elsif @contact.account.blank?
        # contact exists but no account
        token = @contact.generate_token
        @account = @contact.create_account(
            status: 'new',
            login: @contact.email,
            password: token,
            password_confirmation: token )
      end
      @contact.account.send_password_reset
      
      write_log 'check_account', params[ :email ], @contact
      render :account_sent
    end
    
    write_log 'check_account', nil, @contact
  end
  
  def request_password
    if request.post?
      @contact = Contact.find_by email: params[ :email ]
      logger.debug "-- @account:#{@contact.to_yaml}"
      
      unless @contact and @contact.account
        redirect_to login_path, notice: 'E-Mail konnte nicht identifiziert werden' and return
        
      else
        @contact.account.send_password_reset
      end
      
      write_log 'request_password', params[ :email ], @contact
      render :password_sent
    end
    
    write_log 'request_password_account', nil, @contact
  end
  
  def reset_password
    logger.debug "> reset_password @ account -- @account:#{@account.to_yaml} params[ :to ]:#{params[ :to ]}"
    
    unless request.patch?
      if @account.blank? or params[ :to ].blank? or @account.reset_token != params[ :to ]
        write_log 'reset_password', 'incorrect!', @account
        redirect_to root_path, alert: 'Nicht erlaubt!' and return
      end
      if @account.reset_expires_at <= Time.zone.now - RESET_PASSWORD_LINK_VALID_SPAN
        write_log 'reset_password', 'expired!', @account
        redirect_to login_path, alert: 'Der Link ist abgelaufen!' and return
      end
      
    else
      @account.status = 'confirmed' if @account.status == 'new'
      @account.contact.status = 'confirmed' if @account.contact.status = 'new'
      
      if @account.update( password: params[ :account ][ :password ], password_confirmation: params[ :account ][ :password_confirmation ], reset_token: nil )
        write_log 'reset_password', 'successful', @account
        redirect_to login_path, notice: 'Dein Kennwort wurde neu gesetzt, jetzt kannst du dich damit anmelden.'
        
      else
        render :reset_password
      end
    end
    
    write_log 'reset_password_account', nil, @account
  end
  
  def mine
    redirect_to login_path, notice: 'Um die Seite zu sehen, musst du dich einloggen.' and return unless current_account
    
    @account = current_account
    @last_check = @account.last_check
    
    if @account.is_admin?
      @memberships = Membership.where "updated_at > ?", @last_check
      @accounts = Account.where "updated_at > ?", @last_check
      @contacts = Contact.where "updated_at > ?", @last_check
    end
    
    write_log 'mine_account', nil, @account
  end
  
  # GET /personal_home -- without accounts
  def personal_home
    @account = current_account
    @last_check = @account.last_check
  end
  
  # GET/PATCH /accounts/build
  def build
    initialize_session
    @account = Account.new( contact: Contact.new )
    
    # try to extract names from email (login)
    unless session[ :preliminary_account ][ 'login' ].blank?
      name = session[ :preliminary_account ][ 'login' ]&.split( '@' ).first
      @account.contact.first_name = name.capitalize
      
      %w( . _ - ).each do |sep|
        if name&.include? sep
          @account.contact.first_name = name.split( sep ).first.capitalize
          @account.contact.last_name = name.split( sep ).last.capitalize
        end
      end
    end
    
    %w( login first_name last_name plz ).each do |key|
      logger.debug "-- exists_in_session? #{key} - #{exists_in_session? key}"
      render "build_#{key}" and return unless exists_in_session? key
    end
    
    @account = Account.new( session[ :preliminary_account ] )
    @account.contact.email = @account.login
    @account.contact.seed_checker_question
  end
  

  # --- STANDARD CRUD METHODS ------------------
  
  # GET /accounts
  # GET /accounts.json
  def index
    render 'pages/no_access' and return unless h_page_accessible? 'accounts', current_account
    
    @accounts = Account.with_infos.published.shuffle
    
    write_log 'index_accounts'
  end
  
  # GET /accounts/1
  # GET /accounts/1.json
  def show
    require_user unless @account.published?
    
    write_log 'show_account', nil, @account
  end
  
  # GET /accounts/new
  def new
    redirect_to check_accounts_path unless params[ :email ] and params[ :email ] =~ URI::MailTo::EMAIL_REGEXP
    @account = Account.new( login: params[ :email ], contact: Contact.new )
    @account.contact.email = params[ :email ]
    @account.contact.seed_checker_question
  end
  
  # GET /accounts/1/edit
  def edit
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_account_editable?( @account )
    write_log 'edit_account', nil, @account
  end
  
  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new( account_params )
    @account.status = 'new'
    @account.contact.status = 'new' if @account.contact
    
    respond_to do |format|
      if @account.save
        ContactsMailer.with( contact: @account.contact ).
            confirmation_mail.
            deliver_later
        
        format.html { render :create }
        format.json { render :show, status: :created, location: @account }
      else
        @account.contact.seed_checker_question
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'create_account', nil, @account
  end
  
  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_account_editable?( @account )
    
    the_params = account_params
    # if passwort will be set, check old_password
    unless the_params[ :password ].blank?
      the_params[ :old_password ] = 'wrong!' unless @account.authenticate account_params[ :old_password ]
    end
    
    respond_to do |format|
      if @account.update( the_params )
        format.html { redirect_to @account, notice: 'Account wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'update_account', nil, @account
  end
  
  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    write_log 'destroy_account', @account.to_yaml, @account
    @account.destroy
    
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find_by token: params[ :id ]
    render_404 and return if @account.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require( :account ).permit( :login, :password, :old_password, :password_confirmation, :displayname, :label, :description, :picture, :contact_attributes => [ :email, :first_name, :last_name, :plz, :is_member, :read_basics, :read_privacy, :wants_news, :op_a, :op_b, :solved ] )
  end
  
  # --- preliminary account generation
  
  def initialize_session
    session[ :preliminary_account ] ||= {}
    unless params.blank? or params[ :account ].blank?
      account_params.each do |key,value|
        case key
          when 'contact_attributes'
            session[ :preliminary_account ][ 'contact_attributes' ] ||= {}
            value.each { |k,v|
                session[ :preliminary_account ][ 'contact_attributes' ][ k ] = v }
          else
            session[ :preliminary_account ][ key ] = value
        end
      end
    end
  end
  
  def exists_in_session?( in_key )
    return false unless session[ :preliminary_account ]&.is_a? Hash
    
    flat_session = {}
    session[ :preliminary_account ].each do |key,value|
      if value.respond_to? :each
        value.each { |k,v|
            flat_session[ k ] = v }
      else
        flat_session[ key ] = value
      end
    end
    logger.debug "-- flat_session: #{flat_session}"
    !flat_session[ in_key ].blank?
  end
  
  # --- CONSTANTS -----------------------------
  
  RESET_PASSWORD_LINK_VALID_SPAN = 2.hours
  
end
