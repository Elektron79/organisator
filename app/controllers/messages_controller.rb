class MessagesController < ApplicationController
  skip_before_action :require_team, only: %i( new create )
  before_action :require_user, only: %i( new create ) 
  before_action :require_admin, except: %i( new create )
  before_action :set_message, only: %i( show edit update destroy )
  
  # GET /messages
  # GET /messages.json
  def index
    render_404 and return unless params[ :contact_id ]
    
    @contact = Contact.find_by token: params[ :contact_id ]
    @messages = @contact.received_messages.order updated_at: :desc
  end
  
  # GET /messages/1
  # GET /messages/1.json
  def show
  end
  
  # GET /messages/new.js
  def new
    @bare_form = params[ :kind ] == 'bare'
    @message = Message.new
    @message.sender = current_account
    
    case
      when params[ :contact_id ]
        @contact = Contact.find_by token: params[ :contact_id ]
        @message.receiver = @contact
        
      when ( params[ :to ] and params[ :to ] == 'members' )
        @message.receiver_type = 'all'
    end
  end
  
  # GET /messages/1/edit
  def edit
  end
  
  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new( message_params )
    @message.sender = current_account
    
    # check receiver
    if @message.receiver_type == 'all'
      create_all
      
    else
      respond_to do |format|
        if @message.save
          # send the actual message
          ContactsMailer.with( message: @message ).
              message_mail.
              deliver_later
          
          format.html { redirect_to @message, notice: 'Message was successfully created.' }
          format.js
          format.json { render :show, status: :created, location: @message }
        else
          format.html { render :new }
          format.json { render json: @message.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    respond_to do |format|
      if @message.update( message_params )
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_message
    @message = Message.find( params[ :id ] )
    render_404 and return if @message.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def message_params
      params.require( :message ).permit( :sender_id, :sender_type, :receiver_id, :receiver_type, :subject, :content, :to_members, :to_applicants, :to_helpers )
  end
  
  def create_all
    redirect_to login_path, alert: 'Nicht erlaubt.' and return unless h_is_admin?
    
    # select the receivers
    @receivers = []
    @receivers |= Contact.where.not( membership: nil ).select { |c| c.is_member? or c.is_accepted? } if @message.to_members?
    @receivers |= Contact.where.not( membership: nil ).select { |c| c.is_applicant? } if @message.to_applicants?
    @receivers |= Contact.offers_help.reject { |c| c.is_member? or c.is_accepted? or c.is_applicant? } if @message.to_helpers?
    
    # create and send messages
    @sent_messages = []
    
    @receivers.each do |receiver|
      # create message
      message = Message.new( message_params )
      message.sender = current_account
      message.receiver = receiver
      
      if message.save
        # send the actual message
        ContactsMailer.with( message: message ).
            message_mail.
            deliver_later
        @sent_messages << message
        
      else
        logger.debug "-- message.errors:#{message.errors.to_yaml}"
      end
    end
    
    respond_to do |format|
      format.js { render :create_multiple }
    end
  end
  
 end
