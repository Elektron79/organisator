class RegistrationsController < ApplicationController
  before_action :set_registration, only: [ :activate, :show, :edit, :update, :destroy ]
  before_action :set_event, only: %i( new )
  
  skip_before_action :require_team, only: [ :activate, :new, :create ]
  before_action :require_admin, only: %w( index )
  
  # GET /registrations/1/activate
  def activate
    @successful = false
    
    if @registration.secret == params[ :ring ]
      @registration.status = 'confirmed'
      
      # if existing, connect with previous confirmed contact
      previous_contact = Contact.find_by email: @registration.contact.email
      #logger.debug "--- previous_contact:#{previous_contact.to_yaml}"
      
      if previous_contact and previous_contact.created_at < @registration.contact.created_at
        reg_contact = @registration.contact
        logger.debug "--- reg_contact:#{reg_contact.to_yaml}"
        previous_contact.status = 'confirmed'
        @registration.contact = previous_contact
        #logger.debug "--- @registration.contact:#{@registration.contact.to_yaml}"
        
        #a = previous_contact.save
        #logger.debug "--- previous_contact.errors:#{previous_contact.errors.to_yaml}"
        @successful = previous_contact.save and @registration.save
        
        reg_contact.destroy
        
      else
        @registration.contact.status = 'confirmed'
        
        @successful = @registration.contact.save and @registration.save
      end
      
      # notify author of the event
      # TODO!!
    end
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /registrations
  # GET /registrations.json
  def index
    @registrations = Registration.order( created_at: :desc ).limit 50
  end
  
  # GET /registrations/1
  # GET /registrations/1.json
  def show
  end
  
  # GET /registrations/new
  def new
    set_event
    @registration = @event.registrations.build
    @contact = @registration.build_contact
    @contact.wants_news = true
    @contact.seed_checker_question
  end
  
  # GET /registrations/1/edit
  def edit
  end
  
  def create
    @registration = Registration.new( registration_params )
    @registration.status = 'new'
    @registration.contact.status = 'registration'
    
    respond_to do |format|
      unless @registration.event.registration_possible?
        format.html { redirect_to @event, alert: 'Keine Registrierung mehr möglich' and return }
        
      else
        if @registration.save
          ContactsMailer.with( registration: @registration ).
              registration_mail.
              deliver_later
          
          format.html { render :success }
          format.json { render :show, status: :created, location: @registration }
        else      
          @registration.contact.seed_checker_question
          
          format.html { render :new }
          format.json { render json: @registration.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  # PATCH/PUT /registrations/1
  # PATCH/PUT /registrations/1.json
  def update
    respond_to do |format|
      if @registration.update(registration_params)
        format.html { redirect_to @registration, notice: 'Registration was successfully updated.' }
        format.json { render :show, status: :ok, location: @registration }
      else
        format.html { render :edit }
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /registrations/1
  # DELETE /registrations/1.json
  def destroy
    @registration.destroy
    respond_to do |format|
      format.html { redirect_to registrations_url, notice: 'Registration was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_registration
    @registration = Registration.find_by token: params[ :id ]
    render_404 and return if @registration.blank?
  end
  
  def set_event
    if @registration
      @event = @registration.event
    else
      @event = Event.find_by token:  params[ :event_id ].split( '-' ).first
    end
    render_404 and return if @event.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def registration_params
    params.require( :registration ).permit( :event_id, :plz, :city, :emergency_nr, :participants, :accepts_storage, :requests, contact_attributes: [ :email, :first_name, :last_name, :read_privacy, :wants_news, :op_a, :op_b, :solved ] )
  end
  
end
