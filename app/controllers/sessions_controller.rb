# ----------------------------------------------
class SessionsController < ApplicationController
  skip_before_action :require_team,
        only: [ :new, :create, :destroy ]
  
  def new
    write_log 'session new'
  end
  
  def create
    # Entities that might come up in a session:
    # - account_id
    # - contact_id
    # - redirect_target
    account = Account.find_by login: params[ :login ]
    if account.try :authenticate, params[ :password ]
      session[ :account_id ] = account.id
      write_log 'session create'
      
      if session[ :redirect_target ]
        params = session[ :redirect_target ]
        session[ :redirect_target ] = nil
        redirect_to params and return
        
      else
        redirect_to community_path
      end
      
    else
      redirect_to login_url,
            alert: "Unbekannte E-Mail/Password Kombination"
    end
  end
  
  def destroy
    reset_session
    write_log 'session destroy'
    redirect_to root_url, notice: "Du hast Dich abgemeldet"
  end
  
end

