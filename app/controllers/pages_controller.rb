class PagesController < ApplicationController
  before_action :set_page, only: %i{ switchlock show edit update destroy }
  
  skip_before_action :require_team
  before_action :require_editor, except: [ :root, :show, :edit, :impressum, :datenschutz ]
  
  # --- CUSTOM METHODS -------------------------
  
  # GET /root
  def root
    logger.debug "-- request.host:#{request.host}"
    @page = Page.published.find_by path: request.url
    
    write_log 'root_page'
    if @page.blank?
      render :root
    else
      render :show
    end
  end
  
  # PUT /pages/1/switchlock
  def switchlock
    if @page.unlocked?
      new_time = Time.zone.now - Page::UNLOCK_TIMESPAN
    else
      new_time = Time.zone.now 
    end
    
    write_log 'switchlock_page', nil, @page
    @page.update_attribute :unlocked_at, new_time
    redirect_to @page
  end
  
  # --- CRUD METHODS ---------------------------
  
  # GET /pages
  # GET /pages.json
  def index
    render 'pages/no_access' and return unless h_is_editor?
    
    @pages_menu = Page.root_level.in_menu.order position: :asc
    @pages_outside = Page.root_level.out_of_menu.order position: :asc
    
    write_log 'index_pages'
  end
  
  # GET /pages/1
  # GET /pages/1.json
  def show
    # page exists, but access may not be given
    write_log 'show_page', params, @page
    unless @page.accessible_by? current_account
      if h_is_user?
        render :no_access
        
      else
        session[ :redirect_target ] = params
        redirect_to login_path, alert: 'Um diese Seite zu sehen, musst du bestimmte Zugriffsrechte haben.'  and return
      end
    end
  end
  
  # GET /pages/new
  def new
    @page = Page.new
  end
  
  # GET /pages/1/edit
  def edit
    redirect_to @page unless h_is_editor?
  end
  
  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new( page_params )
    redirect_target = params[ :redirect ] || @page
    
    respond_to do |format|
      if @page.save
        format.html { redirect_to redirect_target, notice: 'Seite wurde erfolgreich angelegt.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update( page_params )
        format.html { redirect_to @page, notice: 'Seite wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_page
    if !params[ :path ].blank?
      @page = Page.find_by path: params[ :path ]
    elsif numeric? params[ :id ]
      @page = Page.find params[ :id ]
    else
      @page = Page.find_by path: params[ :id ]
    end
    render_404 and return if @page.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def page_params
    params.require( :page ).permit( :status, :activate_kind, :kind, :parent_id, :path, :title, :menu, :position, :description )
  end
  
end
