class CommunityController < ApplicationController
  skip_before_action :require_team, only: %i( search last_week index show )
  before_action :require_user, only: %i( last_week )
  
  # --- CUSTOM METHODS ------------------------
  
  def search
    @plz = params[ :query ] || '10000'
    
    @near_accounts = Contact.
        includes( :account ).
        where( 'LENGTH( plz ) > 0 AND
            FLOOR( plz / 1000 )= ?',
                @plz[ 0..1 ].to_i ).
        select { |c|
            c.account and
                c.account != current_account }.
        collect { |c| c.account }
    
    @other_accounts = Contact.
        includes( :account ).
        where( 'LENGTH( plz ) > 0 AND
            FLOOR( plz / 10000 )= ?',
                @plz[ 0 ].to_i ).
        select { |c|
            c.account and
                c.account != current_account and
                !@near_accounts.include? c }.
        collect { |c| c.account }
    
    write_log 'search_community', params[ :query ]
  end
  
  # GET /community/last_week
  def last_week
    @accounts = Account.new_last_week
    
    write_log 'last_week_community'
  end
  
  # --- STANDARD CRUD METHODS -----------------
  
  def index
    @new_accounts = Account.new_last_week
    
    write_log 'index_community'
  end
  
  def show
    @plz = params[ :plz ]
    if params[ :circle ] == 'near'
      @description = 'deiner Umgebung'
      @accounts = Contact.
          includes( :account ).
          where( 'LENGTH( plz ) > 0 AND
              FLOOR( plz / 1000 )= ?',
                  @plz[ 0..1 ].to_i ).
          select { |c|
              c.account and
                  c.account != current_account }.
        collect { |c| c.account }
      
    else
      @description = 'anderen Orten'
      @accounts = Contact.
          includes( :account ).
          where( 'LENGTH( plz ) > 0 AND
              FLOOR( plz / 10000 )= ?',
                  @plz[ 0 ].to_i ).
          select { |c|
              c.account and
                  c.account != current_account }.
        collect { |c| c.account }
    end
    
    write_log 'show_community', @plz
  end
  
end

