class DecisionsController < ApplicationController
  before_action :set_decision, only: [ :publish, :vote, :show, :edit, :update, :destroy ]
  skip_before_action :require_team
  before_action :require_decider
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /decisions/1/publish
  def publish
    @decision.status = 'published'
    
    respond_to do |format|
      if @decision.save
        format.html { redirect_to @decision, notice: 'Entscheidung wurde bereit gemacht.' }
        format.json { render :show, status: :published, location: @decision }
      else
        format.html { redirect_to @decision, alert: 'Entscheidung konnte bereit gemacht werden.' }
        format.json { render json: @decision.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'publish_decision', nil, @decision
  end
  
  # PUT /decisions/1/vote
  def vote
    current_account.votes.for_decision( @decision ).each do |vote|
      vote.status = 'published'
      vote.save!
    end
    
    redirect_to @decision, notice: 'Deine Stimmen wurden angenommen.'
  end
  
  # --- INSTANCE METHODS -----------------------
  
  # GET /decisions
  # GET /decisions.json
  def index
    @stage = params[ :stage ]
    @decisions = []
    
    unless @stage
      # no stage was set
      decision = Decision.last_created.first
      @stage = decision ? decision.stage : 'preparation' 
    end
    
    case @stage
      when 'own'
        @decisions = Decision.last_created.where( account: current_account )
      when 'preparation'
        @decisions = Decision.fresh.last_created
      when 'proposing'
        @decisions = Decision.published.last_created.select { |d| d.proposing? }
      when 'voting'
        @decisions = Decision.published.last_created.select { |d| d.voting? }
      when 'done'
        @decisions = Decision.published.last_created.select { |d| d.done? and !d.old? }
      when 'archive'
        @decisions = Decision.published.last_created.select { |d| d.done? and d.old? }
    end
    
    write_log 'index_decisions', @stage
  end
  
  # GET /decisions/1
  # GET /decisions/1.json
  def show
    if @decision.done?
      @decision = Decision.
          where( token: params[ :id ] ).
          references( proposals: [ votes: [ :account ] ] ).
          includes( proposals: [ votes: [ :account ] ] ).first
    end
    
    write_log 'show_decisions', nil, @decision
  end
  
  # GET /decisions/new
  def new
    @decision = Decision.new variant: 'SK', vote_count: 1
    limit = ( Time.zone.now + 1.hour ).beginning_of_hour
    @decision.end_proposals_at = limit + 1.day
    @decision.end_voting_at = limit + 2.days
  end
  
  # GET /decisions/1/edit
  def edit
    write_log 'edit_decision', nil, @decision
  end
  
  # POST /decisions
  # POST /decisions.json
  def create
    @decision = Decision.new( decision_params )
    @decision.account = current_account
    
    respond_to do |format|
      if @decision.save
        format.html { redirect_to @decision, notice: 'Decision was successfully created.' }
        format.json { render :show, status: :created, location: @decision }
      else
        format.html { render :new }
        format.json { render json: @decision.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'create_decision', nil, @decision
  end
  
  # PATCH/PUT /decisions/1
  # PATCH/PUT /decisions/1.json
  def update
    respond_to do |format|
      if @decision.update(decision_params)
        format.html { redirect_to @decision, notice: 'Decision was successfully updated.' }
        format.json { render :show, status: :ok, location: @decision }
      else
        format.html { render :edit }
        format.json { render json: @decision.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'update_decision', nil, @decision
  end
  
  # DELETE /decisions/1
  # DELETE /decisions/1.json
  def destroy
    write_log 'destroy_decision', @decision.to_yaml, @decision
    @decision.destroy
    
    respond_to do |format|
      format.html { redirect_to decisions_url, notice: 'Decision was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_decision
    @decision = Decision.find_by token: params[ :id ]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def decision_params
    params.require( :decision ).permit( :variant, :vote_count, :title, :description, :end_proposals_at, :end_voting_at )
  end
  
end
