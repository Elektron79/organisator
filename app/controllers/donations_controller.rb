class DonationsController < ApplicationController
  before_action :set_donation, only: [ :advance, :show, :edit, :update, :destroy ]
  before_action :set_reward_or_funding, only: [ :new, :create ]
  
  skip_before_action :require_team
  before_action :require_admin, except: [ :new, :create, :show ]
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /donations/1/advance
  # PUT /donations/1/advance.js
  def advance
    @donation.advance_status
    
    redirect_to donations_path( "##{@donation.iid}" )
  end
  
  # --------------------------------------------
  # CRUD methods
  
  # GET /donations
  # GET /donations.json
  def index
    render 'pages/no_access' and return unless h_is_admin?
    
    @donations = Donation.order created_at: :desc
      
    write_log 'index_donations'
  end
  
  # GET /donations/1
  # GET /donations/1.json
  def show
    render_404 unless @donation.contact_id == session[ :contact_id ] or h_is_admin?
    
    write_log 'show_donation', nil, @donation
  end
  
  # GET /donations/new
  def new
    if defined? @reward
      @donation = @reward.donations.build funding: @reward.funding
    else
      @donation = @funding.donations.build
    end
    
    if current_account
      @donation.contact = current_account.contact
    elsif session[ :contact_id ]
      @donation.contact_id = session[ :contact_id ]
    else
      @donation.contact = Contact.new wants_news: true
    end
    
    @donation.amount = params[ :amount ] if request.put? and params[ :amount ]
  end
  
  # GET /donations/1/edit
  def edit
  end
  
  # POST /donations
  # POST /donations.json
  def create
    @donation = @funding.donations.build( donation_params  )
    @donation.status = 'new'
    @donation.contact = current_account.contact if current_account
    if session[ :contact_id ]
      # dont create new contact if it already exists
      @contact = Contact.find session[ :contact_id ]
      @donation.contact = @contact if @contact.email == @donation.contact.email
    end
    
    respond_to do |format|
      if @donation.save
        # save contact_id in session
        session[ :contact_id ] = @donation.contact_id
        
        format.html { redirect_to @donation, notice: 'Spende wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :created, location: @donation }
      else
        format.html { render :new }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /donations/1
  # PATCH/PUT /donations/1.json
  def update
    respond_to do |format|
      if @donation.update( donation_params )
        format.html { redirect_to @donation, notice: 'Donation was successfully updated.' }
        format.json { render :show, status: :ok, location: @donation }
      else
        format.html { render :edit }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /donations/1
  # DELETE /donations/1.json
  def destroy
    @donation.destroy
    respond_to do |format|
      format.html { redirect_to donations_url, notice: 'Finanzierung wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def not_accessible?
    donations_page = Page.find_by path: 'donations'
    return ( donations_page.blank? or donations_page.private? )
  end
  
  def set_donation
    @donation = Donation.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @donation.blank?
  end
  
  def set_reward_or_funding
    redirect_to fundings_path, alert: 'Spende kann nur zu einer Finanzierung erstellt werden.' and return unless params[ :reward_id ] or params[ :funding_id ]
    
    if params[ :reward_id ]
      if numeric?( params[ :reward_id ] )
        @reward = Reward.find params[ :reward_id ].to_i
      else
        @reward = Reward.find_by token: params[ :reward_id ].split( '-' ).first
      end
      @funding = @reward.funding
      
    else
      if numeric? params[ :funding_id ]
        @funding = Funding.find params[ :funding_id ].to_i
      else
        @funding = Funding.find_by token: params[ :funding_id ].split( '-' ).first
      end
    end
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def donation_params
    params.require( :donation ).permit( :status, :amount, :reward_id, contact_attributes: [ :email, :first_name, :last_name, :street, :housenr, :plz, :city, :read_privacy, :wants_news ] )
  end
  
  # --- CONSTANTS ------------------------------
  
end
