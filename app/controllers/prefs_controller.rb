class PrefsController < ApplicationController
  skip_before_action :require_team
  before_action :require_account
  
  before_action :set_pref, only: [ :switch, :edit, :update ]
  
  # --- CUSTOM METHODS -------------------------
  
  # GET /prefs/overview
  def overview
  end
  
  # PUT /prefs/1/switch.js
  def switch
    if current_account.has_status? @pref.level and
        @pref.is_boolean?
      @pref.switch_value
      @pref.save
    end
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /prefs
  # GET /prefs.json
  def index
    @prefs = Pref.where( level: Account.all_status_below( current_account.status ) )
  end
  
  # GET /prefs/1
  # GET /prefs/1.json
  def show
    redirect_to prefs_path
  end
  
  # GET /prefs/1/edit
  def edit
    redirect_to login_path,
            notice: 'Nicht editierbar mit deinen Rechten!' and return unless h_pref_editable?( @pref )
  end
  
  # PATCH/PUT /prefs/1
  # PATCH/PUT /prefs/1.json
  def update
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless current_account.has_status? @pref.level
    
    respond_to do |format|
      if @pref.update( pref_params )
        format.html { redirect_to @pref, notice: 'Pref was successfully updated.' }
        format.json { render :show, status: :ok, location: @pref }
      else
        format.html { render :edit }
        format.json { render json: @pref.errors, status: :unprocessable_entity }
      end
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_pref
    @pref = Pref.find params[ :id ]
    render_404 and return if @pref.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def pref_params
    params.require( :pref ).permit( :key, :value )
  end
  
  # --- CONSTANTS ------------------------------
  
end
