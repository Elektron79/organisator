class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  before_action :extract_token
  before_action :require_team, except: [ :search ]
  before_action :load_pages
  
  after_action :check_mailings
  
  # --- SITE WIDE METHODS ----------------------
  
  def search
    if params[ :q ]
      q = "%#{params[ :q ]}%"
      @results = []
      @results |= Page.where "title LIKE :q OR menu LIKE :q OR description LIKE :q", { q: q }
      @results |= Account.where "displayname LIKE ?", q
      @results |= Article.where [ "title LIKE :q OR description LIKE :q", { q: q } ]
      @results |= Event.where [ "title LIKE :q OR description LIKE :q OR location LIKE :q", { q: q } ]
      
      if h_is_admin?
        @results |= Contact.where [ "first_name LIKE :q OR last_name LIKE :q OR email LIKE :q", { q: q } ]
        @results |= Membership.where [ "token LIKE :q OR city LIKE :q", { q: q } ]
      end
      
      # reject results that should not
      # be accessible
      @results = @results.reject { |r|
            !r.show_to?( current_account ? current_account.status : 'public' ) }
      
      @results.sort! { |a,b| b.updated_at <=> a.updated_at }
    end
    
    render 'general/search'
  end
  
protected
  
  def extract_token
    if params[ :t ]
      logger.debug "> extract_token @ app con -- token:#{params[ :t ]}"
      token = params[ :t ]
      @mailing = Mailing.sent.last_month.detect { |v|
            v.token == params[ :t ] }
      if @mailing
        logger.debug "-- @mailing:#@mailing.to_yaml}"
        @mailing.update_attribute :clicks, @mailing.clicks.to_i + 1
      end
    end
  end
  
  # --- FEATURE & PAGE ENABLING ----------------
  
  def h_app_feature?( in_feature )
    Pref.app_feature? in_feature
  end
  helper_method :h_app_feature?
  
  def h_app_value( in_feature )
    Pref.app_value in_feature
  end
  helper_method :h_app_value
  
  def h_page_defined?( in_path = '' )
    page = Page.find_by path: in_path
    return ( page and page.status != 'new' )
  end
  helper_method :h_page_defined?
  
  def h_page_accessible?( in_path = '', in_account = nil )
    page = Page.find_by( path: in_path )
    return ( ( page and page.accessible_by?( current_account ) ) or h_is_admin? )
  end
  helper_method :h_page_accessible?
  
  # --- ESSENTIALS -----------------------------
  
  def numeric?( in_string )
    Float( in_string ) != nil rescue false
  end
  
  # --- ACCOUNT DETECTION ----------------------
  
  def set_current_account( in_account )
    delete session[ :account_id ] and return unless in_account
    session[ :account_id ] = in_account.id
  end
  
  def current_account
    unless defined? @_current_account
      @_current_account = Account.find_by id: session[ :account_id ]
    end
    return @_current_account
  end
  helper_method :current_account
  
  def h_is_admin?
    current_account and current_account.is_admin?
  end
  helper_method :h_is_admin?
  
  def h_is_editor?
    current_account and current_account.is_editor?
  end
  helper_method :h_is_editor?
  
  def h_is_sales?
    current_account and current_account.is_sales?
  end
  helper_method :h_is_sales?
  
  def h_is_team?
    current_account and current_account.is_team?
  end
  helper_method :h_is_team?
  
  def h_is_member?
    current_account and
        current_account.has_membership? and
        ( current_account.membership.member? or
            current_account.membership.accepted? )
  end
  helper_method :h_is_member?
  
  def h_is_team_or_member?
    current_account and current_account.is_team_or_member?
  end
  helper_method :h_is_team_or_member?
  
  def h_is_internal?
    current_account and current_account.is_internal?
  end
  helper_method :h_is_internal?
    
  def h_is_user?
    current_account and current_account.is_confirmed?
  end
  helper_method :h_is_user?
  
  # --- AUTHORIZATION --------------------------
  
  def require_account
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
  end
  
  def require_user
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du dein Profil durch den Link in der E-Mail bestätigen' and return unless h_is_user?
  end
  
  def require_team_or_member
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    if h_app_feature? :allow_membership
      redirect_to login_path,
            alert: 'Für diese Aktion musst du im Team oder Mitglied sein.' and return unless h_is_team_or_member?
    else
      redirect_to login_path,
            alert: "Für diese Aktion musst du Team-Mitglied sein.\nGib deinem Administrator darüber Bescheid." and return unless h_is_team?
    end
  end
  
  def require_member
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    redirect_to login_path,
            alert: 'Für diese Aktion musst du Mitglied sein.' and return unless h_is_member?
  end
  
  def require_team
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    redirect_to login_path,
            alert: "Für diese Aktion musst du Team-Mitglied sein.\nGib deinem Administrator darüber Bescheid." and return unless h_is_team? # halts request cycle
  end
  
  def require_decider
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    if h_app_feature? :allow_membership
      if h_app_feature? :allow_decisions_for_team
        redirect_to login_path,
              alert: 'Für diese Aktion musst du Mitglied oder Team-Mitglied sein.' and return unless h_is_team?
      else
        redirect_to login_path,
              alert: 'Für diese Aktion musst du Mitglied sein.' and return unless h_is_member?
      end
      
    elsif h_app_feature? :allow_decisions_for_team
      redirect_to login_path,
            alert: "Für diese Aktion musst du Team-Mitglied sein.\nGib deinem Administrator darüber Bescheid." and return unless h_is_team?
    else
      redirect_to root_path,
            alert: 'Entscheidungen sind nicht aktiviert!'
    end
  end
  
  def require_editor
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    redirect_to login_path,
            alert: 'Für diese Aktion musst du Editor sein.' and return unless h_is_editor?
  end
  
  def require_sales
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    redirect_to login_path,
            alert: 'Für diese Aktion musst du im Bestellteam sein.' and return unless h_is_sales?
  end
  
  def require_admin
    session[ :redirect_target ] = params
    redirect_to login_path,
            notice: 'Um diese Seite zu sehen, musst du angemeldet sein' and return unless current_account
    redirect_to login_path,
            alert: 'Für diese Aktion musst du Admin sein' unless h_is_admin?
  end
  
  # --- PAGES ----------------------------------
  
  def load_pages
    logger.debug "-- load_pages @ application controller"
    @pages = restrict_page_access( Page.root_level.order position: :asc )
    @with_sub = nil
    
    unless h_is_team?
      @pages.each do |page|
        sub_pages = restrict_page_access page.pages.in_menu
        @with_sub = 'with_sub' if sub_pages.size > 0
      end
    else
      @with_sub = 'with_sub'
    end
  end
  
  def restrict_page_access( in_pages )
    result = in_pages
    result = result.reject { |p|
        p.status == 'new' }
    result = result.reject { |p|
        p.status == 'team' } unless h_is_team?
    result = result.reject { |p|
        p.status == 'member' } unless h_is_member?
    result = result.reject { |p|
        p.status == 'team_member' } unless h_is_team_or_member?
    result = result.reject { |p|
        p.status == 'editor' } unless h_is_editor?
    result = result.reject { |p|
        p.status == 'admin' } unless h_is_admin?
    return result
  end
  helper_method :restrict_page_access
  
  def page_access_restricted?( in_page )
    return true if in_page.status == 'new'
    return true if in_page.status == 'published' and !h_is_user?
    return true if in_page.status == 'team' and !h_is_team?
    return true if in_page.status == 'member' and !h_is_member?
    return true if in_page.status == 'team_member' and !h_is_team_or_member?
    return true if in_page.status == 'editor' and not !h_is_editor?
    return true if in_page.status == 'admin' and !h_is_admin?
    false
  end
  
  def render_404
    render(
        file: "#{Rails.root}/public/404.html" ,
        status: :not_found,
        layout: false )
  end
  
  # --- MAILINGS -------------------------------
  
  def check_mailings
    logger.debug "\n> check_mailings @ app controller"
    
    # steht ein Newsletter zum Versand an?
    @newsletters = Newsletter.for_mailing
    logger.debug "-- @newsletters:#{@newsletters.size}"
    unless @newsletters.blank?
      @newsletters.each { |n| n.start_mailing }
    end
    
    # stehen Versände aus?
    @mailings = Mailing.to_send
    logger.debug "-- @mailings:#{@mailings.size}"
    unless @mailings.blank?
      logger.debug "-- @mailings.size:#{@mailings.size}"
      @mailings.each { |m| m.send_out }
    end
    
    # are there membership fees to be reminded?
    @members_overdue = Membership.need_fee_check
    logger.debug "-- @members_overdue:#{@members_overdue.to_yaml}"
    unless @members_overdue.blank?
      @members_overdue.each { |m| m.send_fee_request }
    end
    
  end
  
  # --- LOGS -----------------------------------
  
  def write_log( in_action,
        in_comment = nil,
        in_entity = nil )
    logger.debug "> write_log @ app controller"
    
    user_agent = request.env[ 'HTTP_USER_AGENT' ]
    unless user_agent =~ Log::BOT_SIGNATURES
      Log.create actor: current_account,
            action: in_action,
            comment: in_comment,
            entity: in_entity,
            user_agent: ( request.blank? ? nil : user_agent ),
            ip: ( request.blank? ? nil : request.headers[ 'X-Real-IP' ] )
    end
  end
  

end
