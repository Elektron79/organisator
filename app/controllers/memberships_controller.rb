class MembershipsController < ApplicationController
  require 'prawn'
  
  skip_before_action :require_team
  before_action :require_admin, except: %i{ activate show_summary requestpdf show new create edit update }
  
  before_action :check_feature_enabled
  before_action :set_membership, only: %i{ accept activate show_summary requestpdf show edit update destroy }
  
  # --- CUSTOM METHODS -------------------------
  
  # PATCH /memberships/1/accept
  def accept
    @membership.update params.require( :membership ).permit( :starts_at )
    @membership.status = 'accepted'
    
    if @membership.save
      MembershipsMailer.with( membership: @membership ).
            membership_accepted.
            deliver_later if params[ :notify ] == 'yes'
      redirect_to @membership
      
    else
      redirect_to edit_membership_path( @membership ), alert: 'Du musst den Antrag erst korrekt ausfüllen und speichern'
    end
    
    write_log 'accept_membership', nil, @membership
  end
  
  # GET /memberships/1/activate
  def activate
    @successful = false
    
    if @membership.secret == params[ :ring ]
      @membership.status = 'confirmed'
      @membership.contact.status = 'confirmed'
      @membership.starts_at = Time.zone.now
      
      @successful = @membership.contact.save and @membership.save
    end
    
    write_log 'activate_membership', nil, @membership
  end
  
  # GET /memberships/1
  def show_summary
    require_admin_or_owner
  end
  
  # GET /membership/1/requestpdf
  def requestpdf
    membership_doc = make_membership_doc
    
    logger.debug "-- Rails.root: #{Rails.root}"
    to_add_page = CombinePDF.parse( membership_doc ).pages[ 0 ]
    
    template_pdf = CombinePDF.load Rails.root + 'app/assets/images/docs/mitgliedsantrag_blanko.pdf'
    template_pdf.pages.each { |page|
        page << to_add_page }
    
    send_data template_pdf.to_pdf, filename: "mitgliedsantrag_#{@membership.contact.name_text.underscore}.pdf", type: 'application/pdf', disposition: 'inline'
    
    write_log 'requestpdf_membership', nil, @membership
  end
  
  # GET /memberships/export
  def export_v1
    @memberships = Membership.includes :contact, :shares
    
    csv_groups = [ [ '', %w( token status ) ],
        [ 'contact', %w( last_name first_name custodian email ) ],
        [ '', %w( phone_nr gender birth_on street housenr plz city starts_at ends_at ) ],
        [ 'shares.first', %w( count amount ordered_at ) ],
        [ 'contact', %w( remarks ) ]
    ]
    csv_content = csv_groups.
        collect { |g|
            g.last }.
        flatten.
        join "\t"
    csv_content += "\r\n"
    csv_content += @memberships.
        collect { |m|
            csv_groups.
                collect { |g|
                    g.last.collect { |c|
                        g.first.blank? ?
                            m.send( c ) :
                            m.send( g.first ).
                                send( c )
                        }
                    }.
                flatten.
                join "\t" }.
        join "\r\n"
    
    send_data csv_content, filename: "mitglieder_stand_#{Time.zone.now.strftime "%Y%m%d_%H%M"}.tsv", type: :text
  end
  
  def export
    @memberships = Membership.find_by_sql "
        SELECT m.token AS token,
          m.status AS status,
          c.last_name AS last_name,
          c.first_name AS first_name,
          c.custodian AS custodian,
          c.email AS email,
          m.phone_nr AS phone_nr,
          m.gender AS gender,
          m.birth_on AS birth_on,
          m.street AS street,
          m.housenr AS housenr,
          m.plz AS plz,
          m.city AS city,
          m.starts_at AS starts_at,
          m.ends_at AS ends_at,
          s.count AS count,
          s.amount * s.count AS sum_amount,
          p.amount AS payment,
          p.paid_at AS paid_at,
          c.remarks AS remarks
        FROM memberships AS m
          LEFT JOIN contacts AS c ON m.contact_id=c.id
          LEFT JOIN shares AS s ON s.membership_id=m.id
          LEFT JOIN payments AS p ON p.membership_id=m.id
        ORDER BY last_name"
    
    column_titles = %w( token status last_name first_name email phone_nr gender birth_on street housenr plz city starts_at ends_at count sum_amount payment paid_at remarks )
    csv_content = column_titles.
        join( "\t" ) + "\r\n"
    csv_content += @memberships.
        collect { |m|
            column_titles.collect { |c|
                m.send( c ).to_s.
                    gsub( "\r\n", "; " ) }.
            join "\t" }.
        join "\r\n"
    
    send_data csv_content, filename: "mitglieder_stand_#{Time.zone.now.strftime "%Y%m%d_%H%M"}.tsv", type: :text
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /memberships
  # GET /memberships.json
  def index
    @letter_selected = params[ :letter ]
    @letters = Membership.find_by_sql "
        select left( c.last_name, 1 ) as letter
          from memberships as m
            left join contacts as c
            on m.contact_id = c.id
          group by letter
          order by letter asc"
    
    @dates_list = @letter_selected.to_s.length > 1
    @mem_count = Membership.count
    
    case params[ :letter ]
      when 'preliminary', 'completed', 'confirmed', 'status_new', 'accepted', 'cancelled', 'ended'
        @memberships = Membership.
            send( params[ :letter ] ).
            order 'memberships.created_at DESC'
      when 'with_saldo'
        @memberships = Membership.
            send( params[ :letter ] ).
            sort { |a,b| b.created_at <=> a.created_at }
      else
        @memberships = Membership.find_by_sql [ "
            select m.*
              from memberships as m
                left join contacts as c
                on m.contact_id = c.id
              where left( c.last_name, 1 ) = ?
              order by c.last_name", @letter_selected ]
    end
    
    write_log 'index_membership', nil, @membership
  end
  
  # GET /memberships/1
  # GET /memberships/1.json
  def show
    require_admin_or_owner
    
    if h_is_admin?
      render :show
    else
      render :show_member
    end
    
    write_log 'show_membership', nil, @membership
  end
  
  # GET /memberships/new
  def new
    redirect_to current_account.membership and return if h_is_member?
    
    if current_account and !h_is_admin?
      # if not already member, prefill contact
      contact = current_account.contact
      
    else
      contact = Contact.new( wants_news: true )
      contact.seed_checker_question
    end
    
    @membership = Membership.new( step: 1, contact: contact )
    write_log 'new_membership'
  end
  
  # GET /memberships/1/edit
  def edit
    require_admin_or_owner
    
    if h_is_admin?
      render :edit_admin
      
    else
      # ensure membership has contact
      unless @membership.contact
        if current_account
          @membership.contact = current_account.contact
        else
          @membership.build_contact
          @membership.contact.seed_checker_question
        end
      end
      @membership.contact.seed_checker_question
      
      # ensure there is a share when allowed
      if Pref.app_value( :allow_shares ) and @membership.shares.blank?
        @membership.shares.build( { count: 10, amount: Pref.app_value( :share_price ).to_f } )
      end
      
      # increase membership step
      @membership.step += 1
    end
    
    write_log 'edit_membership', nil, @membership
  end
  
  # POST /memberships
  # POST /memberships.json
  def create
    @membership = Membership.new( membership_params )
    unless h_is_admin?
      @membership.status = 'preliminary'
    else
      @membership.status = 'new'
    end
    
    if current_account and
          current_account.contact and
          current_account.contact.email == @membership.contact.email
      @membership.contact = current_account.contact
    else
      @membership.contact.status = 'membership'
    end
    
    if h_is_team_or_member?
      # seed and solve, because we trust
      @membership.contact.op_a = @membership.contact.op_b = 1
      @membership.contact.solved = 2
    end
    
    respond_to do |format|
      if @membership.save
        session[ :membership_token ] = @membership.token
        
        format.html do
          if h_is_admin?
            redirect_to @membership
          else
            redirect_to edit_membership_path( @membership )
          end
        end
        format.json { render :show, status: :created, location: @membership }
        
      else
        # re-seed checker question
        if h_is_team_or_member?
          @membership.contact.op_a = @membership.contact.op_b = 1
          @membership.contact.solved = 2
        else
          @membership.contact.seed_checker_question
        end
        
        format.html { render :new }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'create_membership', nil, @membership
  end
  
  # PATCH/PUT /memberships/1
  # PATCH/PUT /memberships/1.json
  def update
    @membership.update membership_params
    
    membership_done = @membership.step >= 5
    redirect_target = ( membership_done or h_is_admin? ) ? @membership : edit_membership_path( @membership )
    
    if @membership.contact and @membership.contact.new_record?
      @membership.contact.status = 'membership'
    end
    
    respond_to do |format|
      if @membership.save
        if membership_done
          @membership.update_attribute( :status, 'new' ) unless h_is_admin?
          
          if session[ :membership_token ] == @membership.token
            #MembershipsMailer.with( membership: @membership ).
            #confirmation_mail.
            #deliver_later
          end
        end
        
        format.html { redirect_to redirect_target }
        format.json { render :show, status: :ok, location: @order }
        
      else
        @membership.contact.seed_checker_question
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
    
    write_log 'update_membership', nil, @membership
  end
  
  # DELETE /memberships/1
  # DELETE /memberships/1.json
  def destroy
    # save exit date for once and log
    @membership.update_attribute :ends_at, Time.zone.now
    @membership.update_attribute :status, 'ended'
    
    @contact = @membership.contact
    # @membership.destroy -- only status change?
    respond_to do |format|
      format.html { redirect_to @contact, notice: 'Mitgliedschaft wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
    
    write_log 'destroy_membership', nil, @membership
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def check_feature_enabled
    render :not_enabled and return unless h_app_feature? :allow_membership
  end
  
  def set_membership
    @membership = Membership.find_by token: params[ :id ]
    render_404 and return if @membership.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def membership_params
    params.require( :membership ).permit( :step, :gender, :birth_on, :street, :housenr, :plz, :city, :other_memberships, :fee_amount, :fee_period, :fee_mode, :account_owner, :iban, :accepts_debit, :recommendation, :phone_nr, :accepts_rules, :wants_membership, :requested_at, :starts_at, :cancelled_at, :ends_at, :last_payment_at, :request, contact_attributes: [ :id, :email, :first_name, :last_name, :custodian, :wants_news, :offers_help, :read_privacy, :op_a, :op_b, :solved ], shares_attributes: [ :id, :membership_id, :count, :amount ] )
  end
  
  def require_admin_or_owner
    redirect_to root_path, alert: 'Keine Berechtigung für diesen Zugriff' and return unless session[ :membership_token ] == @membership.token or h_is_admin?
  end
  
  def make_membership_doc
    doc = Prawn::Document.new
    #doc.stroke_axis
    doc.font_size 10
    
    # membership number
    doc.bounding_box [ 140, 632.5 ], width: 152, height: 12 do
      #doc.stroke_bounds
      doc.text @membership.token
    end
    
    # membership personal details
    doc.bounding_box [ 35, 575 ], width: 250, height: 100 do
      #doc.stroke_bounds
      doc.text @membership.contact.name_text
      doc.text @membership.street_nr_text
      doc.text @membership.plz_city_text
      doc.text @membership.contact.email
      doc.text "T #{@membership.phone_nr}" unless @membership.phone_nr.blank?
      
      doc.move_down 5
      doc.text "Geburtsdatum: #{I18n.l @membership.birth_on}"
      doc.text "Gesetzliche Vertretung: #{@membership.contact.custodian}" unless @membership.contact.custodian.blank?
    end
    
    # shares and cost text
    doc.bounding_box [ 35, 429 ], width: 250, height: 23 do
      #doc.stroke_bounds
      stext = @membership.shares.first.count.to_s
      stext += " Anteile zu "
      stext += @membership.shares.first.amount.to_s + '€'
      stext += "  plus Aufgebühr von "
      stext += h_app_value( :share_buyers_fee ).to_s
      stext += "%"
      doc.text stext
    end
    
    # shares
    doc.bounding_box [ 384, 404.5 ], width: 16, height: 12 do
      #doc.stroke_bounds
      doc.text @membership.shares.first.count.to_s, align: :center
    end
    
    # cost
    doc.bounding_box [ 363, 392.5 ], width: 57, height: 12 do
      #doc.stroke_bounds
      doc.text ( '%.2f€' % @membership.first_sum_of_all ), align: :center, style: :bold
    end
    
    # tick marks
    doc.bounding_box [ 35, 323 ], width: 250, height: 230 do
      #doc.stroke_bounds
      doc.line_width = 2
      
      doc.move_down 7.5
      make_tick doc if @membership.accepts_rules?
      
      doc.move_down 36.25
      make_tick doc if @membership.contact.wants_news?
      
      doc.move_down 36
      make_tick doc if @membership.contact.offers_help?
      
      doc.move_down 23.25
      make_tick doc if false
      
      doc.move_down 48.5
      make_tick doc if @membership.contact.read_privacy?
      
      doc.move_down 36
      make_tick doc if true
      
    end
    
    # request filed at
    doc.bounding_box [ 140, 68.5 ], width: 250, height: 23 do
      #doc.stroke_bounds
      doc.text I18n.l @membership.created_at, format: :short_day
    end
    
    return doc.render
  end
  
  def make_tick in_doc
    in_doc.stroke do
      in_doc.move_to 2.5, in_doc.cursor + 4
      in_doc.line_to 5.5, in_doc.cursor + 0
      in_doc.line_to 11.5, in_doc.cursor + 8
    end
  end
  
end

