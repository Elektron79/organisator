class CommentsController < ApplicationController
  before_action :set_entity, only: %w{ create }
  before_action :set_comment, only: %w{ destroy }
  
  # --- STANDARD CRUD METHODS -----------------
  
  # POST /comments
  # POST /comments.json
  def create
    @comment = @entity.comments.build( comment_params )
    @comment.status = 'new'
    @comment.account = current_account
    
    respond_to do |format|
      if @comment.save
        entity = @comment.entity
        AccountsMailer.with( comment: @comment ).
            comment_received_mail.
            deliver_later if should_send_comment? @comment.entity
        
        format.html { redirect_to @comment.entity }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @entity = @comment.entity
    @comment.destroy
    
    respond_to do |format|
      format.html { redirect_to @entity, notice: 'Kommentar wurde gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  
  def set_entity
    entity_type = params[ :comment ][ :entity_type ]
    @entity = entity_type.classify.constantize.find params[ :comment ][ :entity_id ]
  end
  
  def set_comment
    @comment = Comment.find params[ :id ]
  end
  
  def comment_params
    params.require( :comment ).permit( :entity_id, :content )
  end
  
  def should_send_comment?( in_entity )
    in_entity and
        in_entity.account and
        ( !in_entity.respond_to?( :wants_comment_notification? ) or in_entity.wants_comment_notification? )
  end
  
end
