class TaggingsController < ApplicationController
  before_action :set_tagging, only: %i{ destroy }
  
  # --- CRUD METHODS ---------------------------
  
  # POST /tags
  # POST /tags.json
  def create
    @tagging = Tagging.new( tagging_params )
    
    respond_to do |format|
      if @tagging.save
        format.html { redirect_to @tagging.entity, notice: 'Tagging was successfully created.' }
        format.json { render :show, status: :created, location: @tagging }
      else
        format.html { render :new }
        format.json { render json: @tagging.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /tags/1
  # DELETE /tags/1.json
  def destroy
    @entity = @tagging.entity
    @tagging.destroy
    
    respond_to do |format|
      format.html { redirect_to @entity, notice: "Tagging was successfully destroyed." }
      format.json { head :no_content }
    end
  end
  
# ---------------------------------------------
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_tagging
    @tagging = Tagging.find params[ :id ]
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def tagging_params
    params.require( :tagging ).permit( :tag_id, :entity_type, :entity_id )
  end
  
end
