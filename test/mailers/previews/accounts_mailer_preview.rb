# Preview all emails at http://localhost:3000/rails/mailers/accounts_mailer
class AccountsMailerPreview < ActionMailer::Preview
  
  # Preview this email at http://localhost:3000/rails/mailers/accounts_mailer/password_reset_mail
  def password_reset_mail
    AccountsMailer.with( account: Account.kast ).password_reset_mail
  end
  
  def comment_received_mail
    AccountsMailer.with( comment: Comment.last ).comment_received_mail
  end
  
end
