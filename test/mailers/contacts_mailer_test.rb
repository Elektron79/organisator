require 'test_helper'

class ContactsMailerTest < ActionMailer::TestCase
  
  test "confirmation_mail" do
    mail = ContactsMailer.confirmation_mail
    assert_equal "Confirmation mail", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end
  
end
