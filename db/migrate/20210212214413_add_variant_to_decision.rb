class AddVariantToDecision < ActiveRecord::Migration[5.2]
  def change
    add_column :decisions, :variant, :string
    add_column :decisions, :vote_count, :integer
  end
end
