class AddStepToMembership < ActiveRecord::Migration[5.2]
  def change
    add_column :memberships, :step, :integer
  end
end
