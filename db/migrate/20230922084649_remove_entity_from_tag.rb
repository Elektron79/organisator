class RemoveEntityFromTag < ActiveRecord::Migration[5.2]
  def change
    remove_reference :tags, :entity, polymorphic: true
  end
end
