class CreateTaggings < ActiveRecord::Migration[5.2]
  def change
    create_table :taggings do |t|
      t.references :tag, foreign_key: true
      t.references :entity, polymorphic: true
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
