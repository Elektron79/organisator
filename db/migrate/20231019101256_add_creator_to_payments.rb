class AddCreatorToPayments < ActiveRecord::Migration[5.2]
  def change
    add_reference :payments, :creator, foreign_key: { to_table: :accounts }
  end
end
