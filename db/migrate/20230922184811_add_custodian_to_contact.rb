class AddCustodianToContact < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :custodian, :string
  end
end
