class AddLockToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :unlocked_at, :datetime
  end
end
