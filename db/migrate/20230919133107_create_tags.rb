class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.string :status
      t.string :label
      t.string :kind
      t.references :entity, polymorphic: true

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
