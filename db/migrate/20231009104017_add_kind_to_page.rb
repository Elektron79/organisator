class AddKindToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :kind, :string
  end
end
