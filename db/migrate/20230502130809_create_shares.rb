class CreateShares < ActiveRecord::Migration[5.2]
  def change
    create_table :shares do |t|
      t.string :status
      t.references :membership, foreign_key: true
      t.integer :count
      t.decimal :amount, precision: 10, scale: 2
      t.datetime :bought_at
      t.datetime :sold_at

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
