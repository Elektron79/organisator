class AddDatesToMemberships < ActiveRecord::Migration[5.2]
  def change
    add_column :memberships, :requested_at, :datetime
    add_column :memberships, :cancelled_at, :datetime
  end
end
