class AddOrderedAtToShare < ActiveRecord::Migration[5.2]
  def change
    add_column :shares, :ordered_at, :datetime
  end
end
