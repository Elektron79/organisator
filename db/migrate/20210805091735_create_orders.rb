class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :token
      t.string :status
      t.references :contact, foreign_key: true
      t.string :customer_kind
      t.string :customer_location
      t.string :delivery_org
      t.string :delivery_name
      t.string :delivery_street
      t.string :delivery_housenr
      t.string :delivery_plz
      t.string :delivery_city
      t.string :delivery_country
      t.string :invoice_org
      t.string :invoice_name
      t.string :invoice_street
      t.string :invoice_housenr
      t.string :invoice_plz
      t.string :invoice_city
      t.string :invoice_country
      t.boolean :read_privacy
      t.boolean :read_terms
      t.string :delivery_status
      t.datetime :delivery_date_at
      t.string :delivery_reference
      t.string :invoice_status
      t.datetime :invoice_date_at
      t.string :invoice_reference
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
