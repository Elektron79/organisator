# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2024_01_18_162921) do

  create_table "accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "login"
    t.string "password_digest"
    t.bigint "contact_id"
    t.string "displayname"
    t.string "label"
    t.text "description"
    t.string "reset_token"
    t.datetime "reset_expires_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_accounts_on_contact_id"
  end

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "answers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "question_id"
    t.bigint "contact_id"
    t.string "openness"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_answers_on_contact_id"
    t.index ["question_id"], name: "index_answers_on_question_id"
  end

  create_table "arguments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.bigint "proposal_id"
    t.string "title"
    t.text "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_arguments_on_account_id"
    t.index ["proposal_id"], name: "index_arguments_on_proposal_id"
  end

  create_table "articles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.bigint "author_id"
    t.string "parent_type"
    t.bigint "parent_id"
    t.string "description"
    t.datetime "published_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_articles_on_author_id"
    t.index ["parent_type", "parent_id"], name: "index_articles_on_parent_type_and_parent_id"
  end

  create_table "comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.string "entity_type"
    t.bigint "entity_id"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_comments_on_account_id"
    t.index ["entity_type", "entity_id"], name: "index_comments_on_entity_type_and_entity_id"
  end

  create_table "contacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "plz"
    t.boolean "wants_news"
    t.boolean "offers_help"
    t.boolean "read_privacy"
    t.boolean "wants_form"
    t.string "street"
    t.string "housenr"
    t.string "city"
    t.string "phone_nr"
    t.text "remarks"
    t.datetime "news_last_sent"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "custodian"
  end

  create_table "contentparts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "container_type"
    t.bigint "container_id"
    t.string "kind"
    t.integer "position"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["container_type", "container_id"], name: "index_contentparts_on_container_type_and_container_id"
  end

  create_table "decisions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.string "title"
    t.text "description"
    t.string "variant"
    t.integer "vote_count"
    t.datetime "end_proposals_at"
    t.datetime "end_voting_at"
    t.datetime "show_results_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_decisions_on_account_id"
  end

  create_table "donations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "funding_id"
    t.bigint "contact_id"
    t.decimal "amount", precision: 10, scale: 2
    t.bigint "reward_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_donations_on_contact_id"
    t.index ["funding_id"], name: "index_donations_on_funding_id"
    t.index ["reward_id"], name: "index_donations_on_reward_id"
  end

  create_table "events", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "description"
    t.string "location"
    t.text "details"
    t.string "url"
    t.integer "max_registrations"
    t.datetime "registration_starts_at"
    t.datetime "registration_ends_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fundings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.string "title"
    t.string "slogan"
    t.text "short_text"
    t.integer "goal_1"
    t.string "goal_1_use"
    t.integer "goal_2"
    t.string "goal_2_use"
    t.integer "goal_3"
    t.string "goal_3_use"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "performance_period"
    t.text "description"
    t.string "category"
    t.string "location"
    t.string "url"
    t.string "some_1"
    t.string "some_2"
    t.string "some_3"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_fundings_on_account_id"
  end

  create_table "logs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "ip"
    t.string "actor_type"
    t.bigint "actor_id"
    t.string "action"
    t.text "comment"
    t.string "entity_type"
    t.bigint "entity_id"
    t.string "user_agent", limit: 500
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actor_type", "actor_id"], name: "index_logs_on_actor_type_and_actor_id"
    t.index ["entity_type", "entity_id"], name: "index_logs_on_entity_type_and_entity_id"
  end

  create_table "mailings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "status"
    t.bigint "contact_id"
    t.string "packet_type"
    t.bigint "packet_id"
    t.datetime "send_at"
    t.integer "clicks"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_mailings_on_contact_id"
    t.index ["packet_type", "packet_id"], name: "index_mailings_on_packet_type_and_packet_id"
  end

  create_table "memberships", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "contact_id"
    t.string "gender"
    t.date "birth_on"
    t.string "street"
    t.string "housenr"
    t.string "plz"
    t.string "city"
    t.text "other_memberships"
    t.decimal "fee_amount", precision: 10, scale: 2
    t.integer "fee_period"
    t.string "fee_mode"
    t.boolean "accepts_debit"
    t.string "account_owner"
    t.string "iban"
    t.boolean "wants_membership"
    t.boolean "accepts_rules"
    t.string "recommendation"
    t.string "phone_nr"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "fee_check_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "step"
    t.datetime "requested_at"
    t.datetime "cancelled_at"
    t.index ["contact_id"], name: "index_memberships_on_contact_id"
  end

  create_table "messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "sender_type"
    t.bigint "sender_id"
    t.string "receiver_type"
    t.bigint "receiver_id"
    t.string "subject"
    t.text "content"
    t.datetime "read_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["receiver_type", "receiver_id"], name: "index_messages_on_receiver_type_and_receiver_id"
    t.index ["sender_type", "sender_id"], name: "index_messages_on_sender_type_and_sender_id"
  end

  create_table "newsletters", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.text "content"
    t.datetime "send_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ordernumbers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "number"
    t.bigint "order_id"
    t.bigint "supplier_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_ordernumbers_on_order_id"
    t.index ["supplier_id"], name: "index_ordernumbers_on_supplier_id"
  end

  create_table "orderquants", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "reward_id"
    t.integer "quantity"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_orderquants_on_order_id"
    t.index ["reward_id"], name: "index_orderquants_on_reward_id"
  end

  create_table "orders", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "category"
    t.integer "step"
    t.bigint "contact_id"
    t.string "customer_kind"
    t.string "customer_location"
    t.string "delivery_org"
    t.string "delivery_name"
    t.string "delivery_street"
    t.string "delivery_housenr"
    t.string "delivery_plz"
    t.string "delivery_city"
    t.string "delivery_country"
    t.boolean "invoice_differs"
    t.string "invoice_org"
    t.string "invoice_name"
    t.string "invoice_street"
    t.string "invoice_housenr"
    t.string "invoice_plz"
    t.string "invoice_city"
    t.string "invoice_country"
    t.boolean "read_privacy"
    t.boolean "read_terms"
    t.string "delivery_status"
    t.datetime "delivery_date_at"
    t.string "delivery_reference"
    t.string "invoice_status"
    t.datetime "invoice_date_at"
    t.string "invoice_reference"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_orders_on_contact_id"
  end

  create_table "owners", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.bigint "account_id"
    t.bigint "funding_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_owners_on_account_id"
    t.index ["funding_id"], name: "index_owners_on_funding_id"
  end

  create_table "pages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "status"
    t.bigint "parent_id"
    t.string "path"
    t.string "title"
    t.string "menu"
    t.integer "position", default: 1, null: false
    t.string "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "kind"
    t.datetime "unlocked_at"
    t.index ["parent_id"], name: "index_pages_on_parent_id"
  end

  create_table "participations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "status"
    t.string "entity_type"
    t.bigint "entity_id"
    t.bigint "participant_id"
    t.datetime "ends_at"
    t.boolean "notify"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["entity_type", "entity_id"], name: "index_participations_on_entity_type_and_entity_id"
    t.index ["participant_id"], name: "index_participations_on_participant_id"
  end

  create_table "payments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "status"
    t.bigint "membership_id"
    t.decimal "amount", precision: 10, scale: 2
    t.datetime "paid_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "creator_id"
    t.index ["creator_id"], name: "index_payments_on_creator_id"
    t.index ["membership_id"], name: "index_payments_on_membership_id"
  end

  create_table "posts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "contact_id"
    t.bigint "stream_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_posts_on_contact_id"
    t.index ["stream_id"], name: "index_posts_on_stream_id"
  end

  create_table "prefs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "status"
    t.string "level"
    t.string "key"
    t.string "description"
    t.string "value"
    t.string "klass"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proposals", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.bigint "decision_id"
    t.string "title"
    t.text "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_proposals_on_account_id"
    t.index ["decision_id"], name: "index_proposals_on_decision_id"
  end

  create_table "questions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "container_type"
    t.bigint "container_id"
    t.bigint "contact_id"
    t.string "category"
    t.string "openness"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_questions_on_contact_id"
    t.index ["container_type", "container_id"], name: "index_questions_on_container_type_and_container_id"
  end

  create_table "registrations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "event_id"
    t.bigint "contact_id"
    t.string "plz"
    t.string "city"
    t.string "emergency_nr"
    t.text "participants"
    t.string "requests"
    t.boolean "accepts_storage"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_registrations_on_contact_id"
    t.index ["event_id"], name: "index_registrations_on_event_id"
  end

  create_table "rewards", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "funding_id"
    t.decimal "price", precision: 10, scale: 2
    t.string "title"
    t.string "category"
    t.integer "available"
    t.text "description"
    t.string "delivery"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["funding_id"], name: "index_rewards_on_funding_id"
  end

  create_table "shares", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "status"
    t.bigint "membership_id"
    t.integer "count"
    t.decimal "amount", precision: 10, scale: 2
    t.datetime "bought_at"
    t.datetime "sold_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "ordered_at"
    t.index ["membership_id"], name: "index_shares_on_membership_id"
  end

  create_table "streams", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.bigint "owner_id"
    t.boolean "public"
    t.integer "max_participants"
    t.boolean "only_invites"
    t.string "status_required"
    t.integer "fade_away_days"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_streams_on_owner_id"
  end

  create_table "suppliers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "name"
    t.bigint "admin_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.index ["admin_id"], name: "index_suppliers_on_admin_id"
  end

  create_table "taggings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "tag_id"
    t.string "entity_type"
    t.bigint "entity_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["entity_type", "entity_id"], name: "index_taggings_on_entity_type_and_entity_id"
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
  end

  create_table "tags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "status"
    t.string "label"
    t.string "kind"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "color"
    t.bigint "parent_id"
    t.index ["parent_id"], name: "index_tags_on_parent_id"
  end

  create_table "votes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3", force: :cascade do |t|
    t.string "status"
    t.bigint "account_id"
    t.bigint "proposal_id"
    t.integer "value"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_votes_on_account_id"
    t.index ["proposal_id"], name: "index_votes_on_proposal_id"
  end

  add_foreign_key "accounts", "contacts"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "answers", "contacts"
  add_foreign_key "answers", "questions"
  add_foreign_key "arguments", "accounts"
  add_foreign_key "arguments", "proposals"
  add_foreign_key "articles", "accounts", column: "author_id"
  add_foreign_key "comments", "accounts"
  add_foreign_key "decisions", "accounts"
  add_foreign_key "donations", "contacts"
  add_foreign_key "donations", "fundings"
  add_foreign_key "donations", "rewards"
  add_foreign_key "mailings", "contacts"
  add_foreign_key "memberships", "contacts"
  add_foreign_key "ordernumbers", "orders"
  add_foreign_key "ordernumbers", "suppliers"
  add_foreign_key "orderquants", "orders"
  add_foreign_key "orderquants", "rewards"
  add_foreign_key "orders", "contacts"
  add_foreign_key "owners", "accounts"
  add_foreign_key "owners", "fundings"
  add_foreign_key "pages", "pages", column: "parent_id"
  add_foreign_key "participations", "contacts", column: "participant_id"
  add_foreign_key "payments", "accounts", column: "creator_id"
  add_foreign_key "payments", "memberships"
  add_foreign_key "posts", "contacts"
  add_foreign_key "posts", "streams"
  add_foreign_key "proposals", "accounts"
  add_foreign_key "proposals", "decisions"
  add_foreign_key "questions", "contacts"
  add_foreign_key "registrations", "contacts"
  add_foreign_key "registrations", "events"
  add_foreign_key "rewards", "fundings"
  add_foreign_key "shares", "memberships"
  add_foreign_key "streams", "accounts", column: "owner_id"
  add_foreign_key "suppliers", "accounts", column: "admin_id"
  add_foreign_key "taggings", "tags"
  add_foreign_key "tags", "tags", column: "parent_id"
  add_foreign_key "votes", "accounts"
  add_foreign_key "votes", "proposals"
end
